package com.adsmarket.util;

import com.adsmarket.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adsmarket.domain.Offer;
import com.adsmarket.dto.OfferDTO;


@Service
@Transactional
public class ConvertUtilImpl implements ConvertUtil {

	@Autowired
	UserService userService;

	@Override
	public OfferDTO convertOffertoDTO(Offer offer, String contextPath) {
		OfferDTO offerDTO = new OfferDTO();
		offerDTO.setId(offer.getId());
		offerDTO.setName(offer.getName());
		offerDTO.setPayout(offer.getPrice());
		offerDTO.setPlatform(offer.getPlatform());
		offerDTO.setImageUrl(offer.getImageUrl());
		offerDTO.setRegion(offer.getRegion());
		offerDTO.setStatus(offer.getStatus());
		offerDTO.setType(offer.getType());
		offerDTO.setIncentType(offer.getIncentType());
		offerDTO.setRequirement(offer.getRequirement());
		offerDTO.setCap(offer.getCap());
		String generatedTrackingLink = contextPath + "/access?userId="+ userService.getLoggedInUser().getId() + "&offerId=" + offer.getId();
		offerDTO.setTrackingLink(generatedTrackingLink);
		return offerDTO;
	}


}
