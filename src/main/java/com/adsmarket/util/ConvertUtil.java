package com.adsmarket.util;

import com.adsmarket.domain.Offer;
import com.adsmarket.dto.OfferDTO;

public interface ConvertUtil {
	OfferDTO convertOffertoDTO(Offer offer, String contextPath);
}
