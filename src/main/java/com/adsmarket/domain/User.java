package com.adsmarket.domain;

import java.io.Serializable;


import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import java.util.Set;

import javax.persistence.*;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.adsmarket.constant.AppConstants;

/**
 * The persistent class for the ads_user database table.
 * 
 */
@Entity
@Table(name = "ads_user")
@NamedQuery(name = "User.findAll", query = "SELECT a FROM User a")
public class User implements UserDetails, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "username")
	private String username;

	@Column(name = "account_type")
	private String accountType;

	@Column(name = "address")
	private String address;

	@Column(name = "bank_account_name")
	private String bankAccountName;

	@Column(name = "bank_account_number")
	private String bankAccountNumber;

	@Column(name = "bank_address")
	private String bankAddress;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "birthday")
	private String birthday;

	@Column(name = "date_of_issue")
	private String dateOfIssue;

	@Column(name = "email")
	private String email;

	@Column(name = "fullname")
	private String fullName;

	@Column(name = "gender")
	private String gender;

	@Column(name = "identity_id")
	private String identityId;

	@Column(name = "phone")
	private String phone;

	@Column(name = "phone_1")
	private String phone1;

	@Column(name = "place_of_issue")
	private String placeOfIssue;

	@Column(name = "referee_id")
	private String refereeId;

	@Column(name = "skype")
	private String skype;

	@Column(name = "password")
	private String password;

	@Column(name = "status")
	private String status;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "country")
	private String country;
	
	@Lob
	@Column(name = "profile_image")
	private byte[] profileImage;
	
	@Column(name = "profile_image_type")
	private String profileImageType;

	@Transient
	private String passwordConfirm;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "joined_date", insertable=false, updatable=false)
	private Date joinDate;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ads_user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Set<Role> roles;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ads_users_offers", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "offer_id", referencedColumnName = "id"))
	private Set<Offer> offers;

	// bi-directional many-to-one association to AdsCpiClick
	@OneToMany(mappedBy = "users")
	private Set<AdsCpiClick> adsCpiClicks;

	// bi-directional many-to-one association to CPIInstall
	@OneToMany(mappedBy = "user")
	private Set<AdsCpiInstall> adsCpiInstalls;

	// bi-directional one-to-one association to AdsBankInfo
	@OneToOne(mappedBy = "user", cascade = { CascadeType.ALL })
	private UserBankInfo bankInfo;
	

	// bi-directional one-to-one association to AdsBankInfo
	@OneToOne(mappedBy = "user", cascade = { CascadeType.ALL })
	private Paypal paypal;

	
	
	
	public byte[] getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(byte[] profileImage) {
		this.profileImage = profileImage;
	}

	public String getProfileImageType() {
		return profileImageType;
	}

	public void setProfileImageType(String profileImageType) {
		this.profileImageType = profileImageType;
	}

	public Paypal getPaypal() {
		return paypal;
	}

	public void setPaypal(Paypal paypal) {
		this.paypal = paypal;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public String getStatus() {
		return status;
	}

	public Set<AdsCpiInstall> getAdsCpiInstalls() {
		return adsCpiInstalls;
	}

	public void setAdsCpiInstalls(Set<AdsCpiInstall> adsCpiInstalls) {
		this.adsCpiInstalls = adsCpiInstalls;
	}

	public UserBankInfo getBankInfo() {
		return bankInfo;
	}

	public void setBankInfo(UserBankInfo bankInfo) {
		this.bankInfo = bankInfo;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<AdsCpiClick> getAdsCpiClicks() {
		return adsCpiClicks;
	}

	public void setAdsCpiClicks(Set<AdsCpiClick> adsCpiClicks) {
		this.adsCpiClicks = adsCpiClicks;
	}

	public Set<Offer> getOffers() {
		return offers;
	}

	public void setOffers(Set<Offer> offers) {
		this.offers = offers;
	}



	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User() {
	}

	public User(String email, String fullName, String gender, String refereeId, String password, String username) {
		super();
		this.email = email;
		this.fullName = fullName;
		this.gender = gender;
		this.refereeId = refereeId;
		this.password = password;
		this.username = username;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountType() {
		return this.accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBankAccountName() {
		return this.bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankAccountNumber() {
		return this.bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankAddress() {
		return this.bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBirthday() {
		return this.birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getDateOfIssue() {
		return this.dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIdentityId() {
		return this.identityId;
	}

	public void setIdentityId(String identityId) {
		this.identityId = identityId;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone1() {
		return this.phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPlaceOfIssue() {
		return this.placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public String getRefereeId() {
		return this.refereeId;
	}

	public void setRefereeId(String refereeId) {
		this.refereeId = refereeId;
	}

	public String getSkype() {
		return this.skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}



	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub

		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		for (Role role : this.roles) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		return grantedAuthorities;
	}


	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return this.username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return this.status == AppConstants.NOT_ACTIVATED ? true : false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return this.status == AppConstants.BANNED ? false : true;
	}

}