package com.adsmarket.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the ads_bank_info database table.
 * 
 */
@Entity
@Table(name="ads_bank_info")
@NamedQuery(name="UserBankInfo.findAll", query="SELECT a FROM UserBankInfo a")
public class UserBankInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@NotEmpty
	@Column(name="bank_acc_name")
	private String bankAccName;

	@NotEmpty
	@Column(name="bank_acc_no")
	private String bankAccNo;

	@Column(name="bank_branch")
	private String bankBranch;

	@NotEmpty
	@Column(name="bank_country")
	private String bankCountry;

	@NotEmpty
	@Column(name="bank_name")
	private String bankName;
	
	@Column(name="swift_code")
	private String swiftCode;
	
	@Column(name="user_id")
	private Integer userId;

	@JsonIgnore
	@Column(name="last_transaction_status")
	private int lastTransactionStatus;

	//bi-directional one-to-one association to AdsUser
	@JsonIgnore
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="user_id", insertable = false, updatable = false)
	private User user;
	
	
	@Column(name="is_enable")
	private Long enable;

	public UserBankInfo() {
		this.enable = 0L;
	}

	
	


	public Long getEnable() {
		return enable;
	}


	public void setEnable(Long enable) {
		this.enable = enable;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}





	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getSwiftCode() {
		return swiftCode;
	}


	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}


	public User getUser() {
		return this.user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBankAccName() {
		return this.bankAccName;
	}

	public void setBankAccName(String bankAccName) {
		this.bankAccName = bankAccName;
	}

	public String getBankAccNo() {
		return this.bankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}

	public String getBankBranch() {
		return this.bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getBankCountry() {
		return this.bankCountry;
	}

	public void setBankCountry(String bankCountry) {
		this.bankCountry = bankCountry;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public int getLastTransactionStatus() {
		return this.lastTransactionStatus;
	}

	public void setLastTransactionStatus(int lastTransactionStatus) {
		this.lastTransactionStatus = lastTransactionStatus;
	}


}