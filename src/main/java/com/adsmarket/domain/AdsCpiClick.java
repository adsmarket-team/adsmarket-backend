package com.adsmarket.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ads_cpi_click database table.
 * 
 */
@Entity
@Table(name="ads_cpi_click")
@NamedQuery(name="AdsCpiClick.findAll", query="SELECT a FROM AdsCpiClick a")
public class AdsCpiClick implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="device_id")
	private String deviceId;

	@Column(name="device_name")
	private String deviceName;

	@Column(name="distributor_id")
	private String distributorId;

	@Column(name="ip")
	private String ip;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "timestamp", insertable=false, updatable=false)
	private Date timestamp;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name="offer_id")
	private int offerId;

	//bi-directional many-to-one association to Offer
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="offer_id" , insertable = false, updatable = false)
	private Offer offers;

	//bi-directional many-to-one association to User
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="user_id", insertable = false, updatable = false)
	private User users;
	
	

	public AdsCpiClick() {
	}

	
	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public int getOfferId() {
		return offerId;
	}


	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return this.deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDistributorId() {
		return this.distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getTimestamp() {
		String dateString = new SimpleDateFormat("yyyy-MM-dd").format(this.timestamp);
		return dateString;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Offer getOffers() {
		return offers;
	}

	public void setOffers(Offer offers) {
		this.offers = offers;
	}

	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}

	

}