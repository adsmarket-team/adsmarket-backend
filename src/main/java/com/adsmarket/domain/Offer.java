package com.adsmarket.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;




/**
 * The persistent class for the ads_offer database table.
 * 
 */
@Entity
@Table(name="ads_offer")
@NamedQuery(name="Offer.findAll", query="SELECT o FROM Offer o")
public class Offer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="cap")
	private String cap;

	@Column(name="currency")
	private String currency;

	@Column(name="description")
	private String description;

	@Column(name="end_time")
	private String endTime;
	
	@JsonIgnore
	@Lob
	@Column(name="image_data")
	private byte[] imageData;

	@JsonIgnore
	@Column(name="image_type")
	private String imageType;

	@Column(name="name")
	private String name;

	@Column(name="original_currency")
	private String originalCurrency;

	@Column(name="original_price")
	private Long originalPrice;

	@Column(name="platform")
	private String platform;

	@Column(name="price")
	private Long price;
	
	@Column(name="region")
	private String region;

	@Column(name="requirement")
	private String requirement;
	
	@Column(name="start_time")
	private String startTime;

	@Column(name="status")
	private String status;

	@Column(name="tracking_link")
	private String trackingLink;

	@Column(name="type")
	private String type;
	
	@Column(name="incent_type")
	private String incentType;
	
	@Column(name="sale")
	private String sale;

	@Column(name="image_url")
	private String imageUrl;
	
	@Column(name="vendor_app_id")
	private String vendorAppId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "timestamp", insertable=false, updatable=false)
	private Date timestamp;

	@JsonIgnore
	@ManyToMany(mappedBy = "offers")
	private Set<User> users;
	
	@JsonIgnore
	@OneToMany(mappedBy="offers", cascade={CascadeType.ALL})
	private Set<AdsCpiClick> adsCpiClicks;
	
	@JsonIgnore
	//bi-directional many-to-one association to Vendor
	@ManyToOne(cascade={CascadeType.ALL})
	private Vendor vendor;
	
	@JsonIgnore
	//bi-directional many-to-one association to CPIInstall
	@OneToMany(mappedBy="offer", cascade={CascadeType.ALL})
	private Set<AdsCpiInstall> adsCpiInstalls;

	public Date getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getVendorAppId() {
		return vendorAppId;
	}

	public void setVendorAppId(String vendorAppId) {
		this.vendorAppId = vendorAppId;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Set<AdsCpiClick> getAdsCpiClicks() {
		return adsCpiClicks;
	}

	public void setAdsCpiClicks(Set<AdsCpiClick> adsCpiClicks) {
		this.adsCpiClicks = adsCpiClicks;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getSale() {
		return sale;
	}

	public void setSale(String sale) {
		this.sale = sale;
	}

	public Offer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCap() {
		return this.cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEndTime() {
		return this.endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public byte[] getImageData() {
		return this.imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}

	public String getImageType() {
		return this.imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOriginalCurrency() {
		return this.originalCurrency;
	}

	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public Long getOriginalPrice() {
		return this.originalPrice;
	}

	public void setOriginalPrice(Long originalPrice) {
		this.originalPrice = originalPrice;
	}

	public String getPlatform() {
		return this.platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public Long getPrice() {
		return this.price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRequirement() {
		return this.requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getStartTime() {
		return this.startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTrackingLink() {
		return this.trackingLink;
	}

	public void setTrackingLink(String trackingLink) {
		this.trackingLink = trackingLink;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIncentType() {
		return incentType;
	}

	public void setIncentType(String incentType) {
		this.incentType = incentType;
	}


}