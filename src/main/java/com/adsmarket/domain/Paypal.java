package com.adsmarket.domain;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

@Entity
@Table(name="ads_user_paypal")
@NamedQuery(name="Paypal.findAll", query="SELECT a FROM Paypal a")
public class Paypal {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="paypal")
	private String paypal;

	@JsonIgnore
	@Column(name="last_transaction_status")
	private int lastTransactionStatus;
	
	@Column(name="user_id")
	private Long userId;
	
	
	@JsonIgnore
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="user_id", insertable = false, updatable = false)
	private User user;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "timestamp")
	private Date timestamp;
	
	@Column(name="is_enable")
	private Long enable;
	
	
	public Paypal() {
		super();
		this.enable = 0L;
	}


	public Long getEnable() {
		return enable;
	}


	public void setEnable(Long enable) {
		this.enable = enable;
	}


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaypal() {
		return paypal;
	}

	public void setPaypal(String paypal) {
		this.paypal = paypal;
	}

	public int getLastTransactionStatus() {
		return lastTransactionStatus;
	}

	public void setLastTransactionStatus(int lastTransactionStatus) {
		this.lastTransactionStatus = lastTransactionStatus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
