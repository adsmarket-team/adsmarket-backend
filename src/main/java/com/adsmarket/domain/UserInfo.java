package com.adsmarket.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ads_user database table.
 * 
 */
@Entity
@Table(name="ads_user")
@NamedQuery(name="UserInfo.findAll", query="SELECT a FROM UserInfo a")
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="account_type")
	private String accountType;

	@Column(name="address")
	private String address;
	
	@Column(name="birthday")
	private String birthday;

	@Column(name="date_of_issue")
	private String dateOfIssue;

	@Column(name="email")
	private String email;

	@Column(name="fullname")
	private String fullName;

	@Column(name="gender")
	private String gender;

	@Column(name="identity_id")
	private String identityId;

	@Column(name="phone")
	private String phone;

	@Column(name="phone_1")
	private String phone1;

	@Column(name="place_of_issue")
	private String placeOfIssue;


	@Column(name="skype")
	private String skype;
	
	@Column(name="username")
	private String username;

	public UserInfo() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIdentityId() {
		return identityId;
	}

	public void setIdentityId(String identityId) {
		this.identityId = identityId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPlaceOfIssue() {
		return placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getUsername() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}


	

}