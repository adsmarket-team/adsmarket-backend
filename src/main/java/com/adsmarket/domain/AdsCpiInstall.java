package com.adsmarket.domain;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ads_cpi_install database table.
 * 
 */
@Entity
@Table(name="ads_cpi_install")
@NamedQuery(name="AdsCpiInstall.findAll", query="SELECT c FROM AdsCpiInstall c")
public class AdsCpiInstall implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="device_id")
	private String deviceId;

	@Column(name="device_name")
	private String deviceName;

	@Column(name="ip")
	private String ip;


	@Column(name="user_earning")
	private Long userEarning;

	@Column(name="publisher_earning")
	private String publisherEarning;
	
	@Column(name="transaction_id")
	private String transactionId;
	
	@Column(name="offer_id", insertable = false, updatable = false)
	private Integer offerId;
	
	@Column(name="user_id", insertable = false, updatable = false)
	private Integer userId;

	//bi-directional many-to-one association to Offer
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="offer_id")
	private Offer offer;

	//bi-directional many-to-one association to AdsUser
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="user_id")
	private User user;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "timestamp", insertable=false, updatable=false)
	private Date timestamp;

	public AdsCpiInstall() {
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return this.deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getTimestamp() {
		String dateString = new SimpleDateFormat("yyyy-MM-dd").format(this.timestamp);
		return dateString;

	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getUserEarning() {
		return this.userEarning;
	}

	public void setUserEarning(Long userEarning) {
		this.userEarning = userEarning;
	}


	public String getPublisherEarning() {
		return publisherEarning;
	}

	public void setPublisherEarning(String publisherEarning) {
		this.publisherEarning = publisherEarning;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}



}