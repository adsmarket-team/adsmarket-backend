package com.adsmarket.controller.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adsmarket.domain.AdsCpiClick;
import com.adsmarket.domain.AdsCpiInstall;
import com.adsmarket.domain.User;
import com.adsmarket.dto.DashboardReportDTO;
import com.adsmarket.repository.AdsCpiClickRepository;
import com.adsmarket.repository.AdsCpiInstallRepository;
import com.adsmarket.service.ReportService;
import com.adsmarket.service.UserService;

@RestController
@RequestMapping("/report")
public class ReportController {
	@Autowired
	UserService userService;
	
	@Autowired
	AdsCpiClickRepository adsCpiClickRepository;
	
	@Autowired
	AdsCpiInstallRepository adsCpiInstallRepository;
	
	@RequestMapping(value = "/getOverallReportData")
	@CrossOrigin
	public @ResponseBody List<DashboardReportDTO> getReportData() {
		try {
			
		
		User user = userService.getLoggedInUser();
		Set<DashboardReportDTO> reportData = new HashSet<DashboardReportDTO>();
		Set<AdsCpiInstall> allInstall = adsCpiInstallRepository.findDistinceByUserId(user.getId());
		Set<AdsCpiClick> allClicks = adsCpiClickRepository.findDistinctByUserId(user.getId());
		

		
		//Get user join Date
		DateTime userJoinedDate = new DateTime(user.getJoinDate());

	
		//Get install count in each day since the user joined
		for (int i = 0; userJoinedDate.isBeforeNow() ; i++ ) {
			userJoinedDate = userJoinedDate.plusDays(1);
			String dateString = new SimpleDateFormat("yyyy-MM-dd").format(userJoinedDate.toDate());
			Long installCount = allInstall.stream().filter(c -> dateString.equals(c.getTimestamp())).count();
			Long totalTargetPayout = allInstall.stream().filter(c -> dateString.equals(c.getTimestamp())).mapToLong(c -> c.getOffer().getPrice()).sum();
			Long totalActualPayout = allInstall.stream().filter(c -> dateString.equals(c.getTimestamp())).mapToLong(c -> c.getOffer().getPrice()).sum();
			Long clickCount = allClicks.stream().filter(c -> dateString.equals(c.getTimestamp())).count();
			DashboardReportDTO reportItem = new DashboardReportDTO();
			reportItem.setDailyTotalInstall(installCount);
			reportItem.setDailyTotalClick(clickCount);
			reportItem.setTargetPayout(totalTargetPayout);
			reportItem.setActualPayout(totalActualPayout);
			reportItem.setDateString(dateString);
			reportItem.setDate(userJoinedDate.toDate());
			reportData.add(reportItem);
			
		}

		List<DashboardReportDTO> reportDataList = new ArrayList<DashboardReportDTO>(reportData);
		Collections.sort(reportDataList);
		
		return reportDataList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
}
