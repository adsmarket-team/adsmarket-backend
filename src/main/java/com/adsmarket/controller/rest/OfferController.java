package com.adsmarket.controller.rest;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties.Session;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adsmarket.domain.Offer;
import com.adsmarket.dto.MyOffersDTO;
import com.adsmarket.service.OfferService;

@RestController
@RequestMapping("/offer")
public class OfferController {
	
	@Autowired
	OfferService offerSerivce;
	
	@RequestMapping(value = "/getActiveOffer")
	@CrossOrigin
	public @ResponseBody Set<Offer> getActiveOffer() {
		return offerSerivce.getAllActiveOffer();
	}
	
	@RequestMapping(value = "/getOffer")
	@CrossOrigin
	public @ResponseBody Offer getOffer(@RequestParam int id) {
		return offerSerivce.getOfferById(id);
	}
	
	@RequestMapping(value = "/getMyOffers")
	@CrossOrigin
	public @ResponseBody MyOffersDTO getMyOffers(@RequestParam int userId) {
		return offerSerivce.getMyOffers(userId) ;
	}
	
	
	@RequestMapping(value = "/assignOffer")
	@CrossOrigin
	public @ResponseBody boolean assignOffer(@RequestParam int userId, @RequestParam int offerId) {
		return offerSerivce.assignOffer(userId, offerId);
	}
	

}
