package com.adsmarket.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adsmarket.domain.UserBankInfo;
import com.adsmarket.service.UserProfileService;

import com.adsmarket.service.UserService;

@RestController
@RequestMapping("/userProfile")
public class UserProfileController {
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	UserService userService;
	
//	@RequestMapping(value = "/getUserDesc")
//	@CrossOrigin
//	public @ResponseBody UserInfo getUserDesc(@RequestParam(required = true) String userName) {
//		return userProfileService.getUserDesc(userName);
//	}
	
//	@RequestMapping(value = "/updateUserDesc",  method = RequestMethod.POST)
//	@CrossOrigin
//	public boolean updateUserDesc(@RequestBody UserInfo userInfo) {
//		try {
//			return userProfileService.updateUserDesc(userInfo, );
//		}
//		catch (Exception e) {
//			return false;
//		}
//	
//	
//	}
	
	@RequestMapping(value = "/getBankInfo")
	@CrossOrigin
	public @ResponseBody UserBankInfo getBankInfo(@RequestParam(required = true) Integer userId) {
		return userProfileService.getUserBankInfo(userId);
	}
	
	@RequestMapping(value = "/updateBankInfo")
	@CrossOrigin
	public boolean updateBankInfo(@RequestBody UserBankInfo bankInfo) {
		return userProfileService.updateUserBankInfo(bankInfo);
	}
	


}
