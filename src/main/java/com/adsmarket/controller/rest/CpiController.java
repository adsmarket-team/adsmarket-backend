package com.adsmarket.controller.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.adsmarket.service.CpiServiceImpl;
import com.adsmarket.service.OfferService;

@RestController
public class CpiController {

	
	@Autowired
	CpiServiceImpl cpiService;
	
	
	@RequestMapping(value = "/access")
	@CrossOrigin
	public ModelAndView cpi(@RequestParam(required = true) int userId, 
			@RequestParam(required = true) int offerId, HttpServletRequest request) {

		try {
		String ip = request.getRemoteAddr();
		
		boolean isUserValid = cpiService.userValidation(userId);


		boolean isOfferValid = cpiService.offerValidation(offerId, userId);

		//IP Validation
		boolean isRequestIPValid = cpiService.checkOfferRecordedClickIp(userId, offerId, ip);
		
		if (isUserValid && isOfferValid && isRequestIPValid ) {
			if (cpiService.recordClick(offerId, userId, ip)) {
				String redirectURL = cpiService.redirectOffer(offerId, userId);
				if (redirectURL != null) {
					return new ModelAndView("redirect:" + redirectURL);
				}
				else {
					return null;
				}
				
			}
		} else {
			return null;
		}
		} catch (Exception e) {
			return null;
		}

		return null;
		
	}
	
	//Postback handling 
	
	////Adsota Postback
	@RequestMapping(value = "/postback", method = RequestMethod.POST)
	@CrossOrigin
	public ResponseEntity<?> postbackHandler(
			@RequestParam(value = "publisher_id", required = true) String accId,
			@RequestParam(value = "campaign_id",required = true) String vendorAppId,
			@RequestParam(value = "device_id",required = true) String deviceId,
			@RequestParam(value = "device_name",required = true) String deviceName,
			@RequestParam(value = "ip",required = true) String ip,
			@RequestParam(value = "earning",required = true) String earning,
			@RequestParam(value = "currency",required = true) String currency,
			@RequestParam(value = "transaction_id",required = true) String transactionId,
			@RequestParam(value = "timestamp",required = true) String timestamp,
			@RequestParam(value = "param1",required = true) Integer userId,
			@RequestParam(value = "param2",required = true) Integer offerId,
			@RequestParam(value = "param3",required = true) Integer vendorId
) {
		try {
			boolean isPostbackValid = cpiService.postbackValidation(accId,vendorAppId,vendorId, userId, offerId, ip, deviceId, deviceName, transactionId);
			if (isPostbackValid) {
				cpiService.recordInstall(userId, offerId, ip, deviceId, deviceName, earning, transactionId ) ;
				return new ResponseEntity<>(HttpStatus.OK);
		} else {
			 throw new Exception();
		}
		} catch (Exception e) {
			 String errorMessage;
		     errorMessage = e.getMessage() + " <==== Postback is invalid";
		     return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
		}
		
		
		
	}

}
