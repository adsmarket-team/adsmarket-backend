package com.adsmarket.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adsmarket.constant.AppConstants;
import com.adsmarket.domain.Offer;
import com.adsmarket.dto.OfferDTO;
import com.adsmarket.dto.SearchDTO;
import com.adsmarket.service.OfferService;
import com.adsmarket.util.ConvertUtil;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/campaign")
public class CampaignController {
	@Autowired
	OfferService offerService;

	@Autowired
	ConvertUtil convertUtil;



	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String campaign(Model model, HttpServletRequest request) {
		model.addAttribute("viewName", AppConstants.VIEW_CAMPAIGN_ALL);
		Set<Offer> offers = offerService.getAllOffer();
		Set<OfferDTO> offerDTOs = new HashSet<OfferDTO>();
		for (Offer offer : offers) {
			offerDTOs.add(convertUtil.convertOffertoDTO(offer,request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()));
		}
		model.addAttribute("listOfferDTO", offerDTOs);
		model.addAttribute(new SearchDTO());
		return "site.campaign.all";
	}

	@RequestMapping(value = "/getOfferDetail", method = RequestMethod.POST)
	public @ResponseBody OfferDTO getOfferDetail(@RequestParam(name = "offerId") Integer offerId, HttpServletRequest request) {
		try {
			Offer offer = offerService.getOfferById(offerId);
			return convertUtil.convertOffertoDTO(offer, request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	@RequestMapping(value = "/multiSearch", method = RequestMethod.POST)
	public ModelAndView multiSearch(@RequestBody SearchDTO searchDTO, HttpServletRequest request) {
	
		return null;

	}
}
