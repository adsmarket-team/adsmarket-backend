package com.adsmarket.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import com.adsmarket.domain.UserBankInfo;
import com.adsmarket.constant.AppConstants;
import com.adsmarket.domain.Paypal;
import com.adsmarket.domain.User;
import com.adsmarket.domain.UserInfo;
import com.adsmarket.dto.PasswordDTO;
import com.adsmarket.repository.PaypalRepository;

import com.adsmarket.service.UserProfileService;
import com.adsmarket.service.UserService;
import com.adsmarket.validator.UpdatePasswordValidator;


@Controller
@RequestMapping(value = "/profile")
public class ProfileController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserProfileService userProfileService;
	
	@Autowired
	private UpdatePasswordValidator updatePasswordValidator;
	
	@Autowired
	private PaypalRepository paypalRepository;
	

	
    @RequestMapping(value = "", method=RequestMethod.GET)
  	public String profile(Model model) {
    	
    	User loggedInUser = userService.getLoggedInUser();
    	Integer currentUserId = loggedInUser.getId();
    	UserInfo userInfo = userProfileService.getUserDesc(currentUserId);
    	PasswordDTO passwordInfo = new PasswordDTO();
    	UserBankInfo userBankInfo = loggedInUser.getBankInfo();
    	Paypal paypal = loggedInUser.getPaypal();
    	
    	if (userBankInfo == null) {
    		userBankInfo = new UserBankInfo();
    	}
    	if (paypal == null) {
    		paypal = new Paypal();
    	}
    	model.addAttribute(userInfo);
    	model.addAttribute(passwordInfo);
    	model.addAttribute(userBankInfo);
    	model.addAttribute("paypalDTO", paypal);
    	model.addAttribute("viewName", AppConstants.VIEW_PROFILE);
    	model.addAttribute("subViewName", AppConstants.SUBVIEW_PROFILE_PERSONAL);

  	    return "site.profile";
  	}
    
    @RequestMapping(value = "/payment", method=RequestMethod.GET)
  	public String payment(Model model) {
    	model.addAttribute(new UserBankInfo());
    	model.addAttribute(new Paypal());
    	model.addAttribute("viewName", AppConstants.VIEW_PROFILE);
    	model.addAttribute("subViewName", AppConstants.SUBVIEW_PROFILE_PAYMENT);
  	    return "site.profile.payment";
  	}
    

    @RequestMapping(value = "/payment/bankUpdate", method=RequestMethod.POST)
   	public @ResponseBody boolean bankUpdate(@Valid @RequestBody UserBankInfo userBankInfo, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		return false;
    	} else {	
    		try {
    		userBankInfo.setUserId(userService.getLoggedInUser().getId());
    		userProfileService.updateUserBankInfo(userBankInfo);
    		return true;
    		} catch (Exception e) {
    			return false;
    		}
    	}

   	}
    
    @RequestMapping(value = "/payment/paypalUpdate", method=RequestMethod.POST)
   	public @ResponseBody boolean paypalUpdate(@RequestBody Paypal paypal) {
    	try {
    		User loggedInUser = userService.getLoggedInUser();
    		Paypal currentPaypal = loggedInUser.getPaypal();
    		if (currentPaypal == null) {
    			currentPaypal = paypal;
    			currentPaypal.setUser(loggedInUser);
        		loggedInUser.setPaypal(currentPaypal);
    		} else {
    			currentPaypal.setPaypal(paypal.getPaypal());
    		}
    		paypalRepository.saveAndFlush(currentPaypal);
    		return true;  		
    		
    		} catch (Exception e) {
    			return false;
    		}
   	}
    
    
    @RequestMapping(value = "/password", method=RequestMethod.GET)
  	public String password(Model model) {
    	
    	PasswordDTO passwordDTO = new PasswordDTO();
    	model.addAttribute(passwordDTO);
    	model.addAttribute("viewName", AppConstants.VIEW_PROFILE);
    	model.addAttribute("subViewName", AppConstants.SUBVIEW_PROFILE_PASSWORD);

  	    return "site.update.password";
  	}
    
    
    @RequestMapping(value = "/updateUserDesc",  method = RequestMethod.POST)
	@CrossOrigin
	public boolean updateUserDesc(@RequestBody UserInfo userInfo) {
		try {
			return userProfileService.updateUserDesc(userInfo, userService.getLoggedInUser());
		}
		catch (Exception e) {
			return false;
		}
	
	
	}
    
    @RequestMapping(value = "/updatePassword", method=RequestMethod.POST)    
   	public String updatePassword(@ModelAttribute("passwordDTO") @Valid PasswordDTO passwordDTO, BindingResult bindingResult, Model model, HttpServletRequest request ) {
    	try {
    		updatePasswordValidator.validate(passwordDTO, bindingResult);
    	 if (bindingResult.hasErrors()) {
             return "site.update.password";
         }
    	 else {
    		 User loggedInUser = userService.getLoggedInUser();
    		 loggedInUser.setPassword(passwordDTO.getNewPassword());
    		 loggedInUser.setPasswordConfirm(passwordDTO.getrNewPassword());
    		 userService.saveAndFlush(loggedInUser);
    		 return "redirect:/logout";

    	 }
    	} catch (Exception e) {
    		return "404.error";
    	}
    }
    
    @RequestMapping(value = "/changeAvatar", method=RequestMethod.POST) 
    public @ResponseBody boolean updateAvatar(@RequestParam("profileImage") MultipartFile profileImage) {
    	System.out.println(profileImage.toString());
    	InputStream input;
    	User loggedInUser = userService.getLoggedInUser();
		try {
			input = profileImage.getInputStream();
			byte[] image = IOUtils.toByteArray(input); // Apache commons IO.
			loggedInUser.setProfileImage(image);
			userService.saveAndFlush(loggedInUser);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
		 	return false;
		}
    	


   
    }
}
