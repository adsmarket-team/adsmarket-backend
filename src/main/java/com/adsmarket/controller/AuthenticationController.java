package com.adsmarket.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

import com.adsmarket.domain.User;

import com.adsmarket.dto.RegistrationFormDTO;
import com.adsmarket.service.security.AuthenticationService;
import com.adsmarket.service.security.SecurityService;
import com.adsmarket.validator.RegisterValidator;


@Controller
public class AuthenticationController {
    
    
    @Autowired
    private RegisterValidator registerValidator;
    
    @Autowired
    private AuthenticationService authenticationService;
    
    @Autowired
    private SecurityService securityService;
    

    @RequestMapping(value = "/login", method=RequestMethod.GET)    
   	public String login(HttpServletRequest request ) {
    	Object userDetails = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (userDetails instanceof UserDetails) {
        	  	return "site.home";
        }

        return "site.login";
   	   
    }
    
    @RequestMapping(value = "/register", method=RequestMethod.GET)    
   	public String register(Model model) {	
    	model.addAttribute("registerForm", new RegistrationFormDTO());
		return "site.register";

   	   
    }

    @RequestMapping(value = "/register", method=RequestMethod.POST)    
   	public String register(@ModelAttribute("registerForm") @Valid RegistrationFormDTO registerForm, BindingResult bindingResult, Model model, HttpServletRequest request ) {
    	try {
    	registerValidator.validate(registerForm, bindingResult);
    	 if (bindingResult.hasErrors()) {
             return "site.register";
         }
    	 else {
    		 User newUser = authenticationService.register(registerForm);
    		 securityService.autologin(registerForm.getEmail(), registerForm.getPassword(), request);
    		 return "site.home";
    	 }
    	} catch (Exception e) {
    		return "404.error";
    	}
    }
    
    @RequestMapping(value = "/resetPassword", method=RequestMethod.GET)    
   	public String resetPass(Model model) {	

		return "site.reset.password";

   	   
    }
    
}
