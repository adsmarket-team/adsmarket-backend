package com.adsmarket.controller;

import com.adsmarket.dto.DashboardReportDTO;
import com.adsmarket.service.ReportService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.adsmarket.constant.AppConstants;
import com.adsmarket.dto.GlobalPageDTO;

/**
 * Controller that demonstrates tiles mapping, reguest parameters and path variables.
 * 
 * @author Mark Meany
 */
@Controller
public class MainController {
	private Log log = LogFactory.getLog(this.getClass());
	@Autowired
	ReportService reportService;

    @RequestMapping(value = {"/", "/home", "/index", "/welcome", "/dashboard"}, method=RequestMethod.GET)
	public String home(Model model) {
//    	GlobalPageDTO globalPageDTO = new GlobalPageDTO();
//    	globalPageDTO.setViewName(AppConstants.HOME);
		DashboardReportDTO dashboardReportDTO = new DashboardReportDTO();
		dashboardReportDTO.setMonthlyTotalClick(reportService.getTotalMonthlyClick());
		dashboardReportDTO.setMonthlyTotalInstall(reportService.getTotalMonthlyInstall());
		dashboardReportDTO.setActualPayout(null);
    	model.addAttribute("subViewName", "");
		model.addAttribute("dashboardReportDTO", dashboardReportDTO);
	    return "site.home";
	}
    
 
    
    
    
    @RequestMapping(value = "/faq", method=RequestMethod.GET)
  	public String faq() {
  	    return "site.faq";
  	}
    
    @RequestMapping(value = "/contact", method=RequestMethod.GET)
   	public String contact() {
   	    return "site.contact";
   	}
    
	
	@RequestMapping(value = "/greet", method=RequestMethod.GET)
	public ModelAndView greet(@RequestParam(value = "name", required=false, defaultValue="World!")final String name, final Model model) {
		log.info("Controller has been invoked with Request Parameter name = '" + name + "'.");
		return new ModelAndView("site.greeting", "name", name);
	}

	@RequestMapping(value = "/greet/{name}", method=RequestMethod.GET)
	public ModelAndView greetTwoWays(@PathVariable(value="name") final String name, final Model model) {
		log.info("Controller has been invoked with Path Variable name = '" + name + "'.");
		return new ModelAndView("site.greeting", "name", name);
	}
}
