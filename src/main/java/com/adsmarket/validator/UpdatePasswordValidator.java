package com.adsmarket.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.adsmarket.domain.User;
import com.adsmarket.dto.PasswordDTO;
import com.adsmarket.service.UserService;

@Component
public class UpdatePasswordValidator implements Validator {
    @Autowired
    private UserService userService;
    
    @Autowired
    private BCryptPasswordEncoder encoder;
    
	

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PasswordDTO passwordDTO = (PasswordDTO) o;
        
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPassword", "NotEmpty");
        if (!encoder.matches(passwordDTO.getOldPassword(),userService.getLoggedInUser().getPassword())) {
            errors.rejectValue("oldPassword", "Invalid.passwordInfo.oldPassword");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "NotEmpty");
        if (passwordDTO.getNewPassword().length() < 8 || passwordDTO.getNewPassword().length() > 32) {
            errors.rejectValue("newPassword", "Size.registerForm.password");
        }
        
        if (!passwordDTO.getNewPassword().equals(passwordDTO.getrNewPassword())) {
     
        		 errors.rejectValue("rNewPassword", "Diff.registerForm.rpassword");
 
        }

    }
}
