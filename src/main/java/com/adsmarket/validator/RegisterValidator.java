package com.adsmarket.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.adsmarket.domain.User;
import com.adsmarket.dto.RegistrationFormDTO;
import com.adsmarket.service.UserService;

@Component
public class RegisterValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegistrationFormDTO registrationForm = (RegistrationFormDTO) o;
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (userService.findByEmail(registrationForm.getEmail()) != null) {
            errors.rejectValue("email", "Existed.registerForm.email");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (registrationForm.getUsername().length() < 6 || registrationForm.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.registerForm.username");

        }
        if (userService.findByUserName(registrationForm.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.registerForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (registrationForm.getPassword().length() < 8 || registrationForm.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.registerForm.password");
        }
        
        if (registrationForm.getReferee() != null && !registrationForm.getReferee().isEmpty()) {
        	if (userService.findByUserName(registrationForm.getReferee()) == null) {
        		 errors.rejectValue("referee", "NotExisted.registerForm.referee");
        	}
        }

    }
}
