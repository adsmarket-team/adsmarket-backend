package com.adsmarket.service;

import java.io.File;
import java.net.InetAddress;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adsmarket.constant.AppConstants;
import com.adsmarket.domain.AdsCpiClick;
import com.adsmarket.domain.AdsCpiInstall;
import com.adsmarket.domain.Offer;
import com.adsmarket.domain.User;
import com.adsmarket.domain.Vendor;
import com.adsmarket.repository.AdsCpiClickRepository;
import com.adsmarket.repository.AdsCpiInstallRepository;
import com.adsmarket.repository.OfferRepository;
import com.adsmarket.repository.UserRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.Country;


@Service
@Transactional
public class CpiServiceImpl implements CpiService {

	private String ERRx00001 = "ERRx00001";
	private String ERRx00002 = "ERRx00002";
	private String ERRx00003 = "ERRx00003";
	private String OK = "OK";

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OfferRepository offerRepository;

	@Autowired
	private AdsCpiClickRepository adsCpiClickRepository;

	@Autowired
	private AdsCpiInstallRepository adsCpiInstallRepository;

	@Autowired
	private OfferService offerService;

	@Override
	public boolean userValidation(int userId) {
		User user = userRepository.findById(userId);
		if (user == null) {
			return false;
		} else if (AppConstants.BANNED.equals(user.getStatus())) {
			return false;
		} else if (AppConstants.NOT_ACTIVATED.equals(user.getStatus())) {
			return false;
		}
		return true;
	}

	@Override
	public boolean offerValidation(int offerId, int userId){
		Offer offer = offerRepository.findOne(offerId);
		User user = userRepository.findOne(userId);
		Set<Offer> userOffers = user.getOffers();
		if (offer.getStatus().equals(AppConstants.ACTIVE)) {
			if ( userOffers.contains(offer) ) {
				return true;
			} else {
				offerService.assignOffer(userId, offerId);
				return true;
			}
		} else {
			return false;
		}

	}

	@Override
	public boolean checkOfferRecordedClickIp(int userId, int offerId, String ip) {
		if (adsCpiClickRepository.findDistinctByUserIdAndOfferIdAndIp(userId, offerId, ip).size() == 0) {
			Offer offer = offerRepository.findOne(offerId);
			String region = offer.getRegion();
			String[] regions = region.split(", ");
			String requestLocation = getLocation(ip);
			if (requestLocation != null ) {
				for (String item: regions) {
					if (item.equals(requestLocation)) {
						return true;
					}
				}
				return false;
			} else {
				return false;
			}
			
		} else {
			return false;
		}

	}

	@Override
	public boolean recordClick(int offerId, int userId, String ip) {
		AdsCpiClick click = new AdsCpiClick();
		click.setUserId(userId);
		click.setOfferId(offerId);
		click.setIp(ip);
		try {
			adsCpiClickRepository.saveAndFlush(click);
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public String redirectOffer(Integer offerId, Integer userId) {
		Offer offer = offerRepository.findOne(offerId);
		if (offer != null) {
			Vendor vendor = offer.getVendor();
			if (vendor != null && vendor.getId() == AppConstants.ADSOTA) {
				return vendor.getUrl() + "?pid=" + vendor.getAccId() + "&app=" + offer.getVendorAppId() + "&sub1="
						+ userId + "&sub2=" + offerId + "&sub3=" + offer.getVendor().getId();
			}
		} else {
			return null;
		}

		return null;
	}

	@Override
	public boolean postbackValidation(String accId, String vendorAppId, Integer vendorId, Integer userId,
			Integer offerId, String ip, String deviceId, String deviceName, String transactionId) {
		// TODO Auto-generated method stub

		boolean isUserValid = this.userValidation(userId);
		boolean isOfferValid = this.offerValidation(offerId, userId);
		boolean isInstallValid = adsCpiInstallRepository
				.findDistinctByUserIdAndOfferIdAndDeviceNameOrDeviceIdOrIp(userId, offerId, deviceName, deviceId, ip)
				.size() == 0 ? true : false;
		

		Offer offer = offerRepository.findByIdAndVendorAppIdAndVendorId(offerId, vendorAppId, vendorId);
		if (offer != null && offer.getStatus().equals(AppConstants.ACTIVE)) {
			Vendor vendor = offer.getVendor();
			if (vendor != null && vendor.getAccId().equals(accId)) {
				if (isUserValid && isOfferValid && isInstallValid) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	@Override
	public void recordInstall(Integer userId, Integer offerId, String ip, String deviceId, String deviceName,
			String earning, String transactionId) {
		// TODO Auto-generated method stub

		AdsCpiInstall install = new AdsCpiInstall();
		install.setDeviceId(deviceId);
		install.setDeviceName(deviceName);
		install.setIp(ip);
		install.setUserEarning(offerRepository.findOne(offerId).getPrice());
		install.setPublisherEarning(earning);
		install.setUser(userRepository.findOne(userId));
		install.setOffer(offerRepository.findOne(offerId));
		install.setTransactionId(transactionId);
		adsCpiInstallRepository.saveAndFlush(install);

	}

	public String getLocation(String ip) {
		try {
			
	
		// A File object pointing to your GeoIP2 or GeoLite2 database
		ClassPathResource resource = new ClassPathResource("GeoLite2-Country.mmdb");
		
		File database = resource.getFile();

		// This creates the DatabaseReader object, which should be reused across
		// lookups.
		DatabaseReader reader = new DatabaseReader.Builder(database).build();

		InetAddress ipAddress = InetAddress.getByName(ip);

		// Replace "city" with the appropriate method for your database, e.g.,
		// "country".
		CountryResponse response = reader.country(ipAddress);

		Country country = response.getCountry();
		return country.getIsoCode();

		} catch (Exception e) {
			return null;
		}


}
}
