package com.adsmarket.service;


import com.adsmarket.domain.UserBankInfo;
import com.adsmarket.domain.User;
import com.adsmarket.domain.UserInfo;


public interface UserProfileService {
	
	UserInfo getUserDesc(Integer userId);
	boolean updateUserDesc(UserInfo userInfo, User user);
	UserBankInfo getUserBankInfo(Integer userId);
	boolean updateUserBankInfo(UserBankInfo bankInfo);


}
