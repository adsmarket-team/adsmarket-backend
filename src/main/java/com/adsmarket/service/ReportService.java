package com.adsmarket.service;

/**
 * Created by quanh on 5/25/2017.
 */
public interface ReportService {
    Long getTotalMonthlyClick();
    Long getTotalMonthlyInstall();
    Long getTotalMonthlyConversion();
    Long getTotalDailyClick();
    Long getTotalDailyInstall();
}
