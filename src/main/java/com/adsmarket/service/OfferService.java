package com.adsmarket.service;

import java.util.Set;

import com.adsmarket.domain.Offer;
import com.adsmarket.dto.MyOffersDTO;

public interface OfferService {
	Set<Offer> getAllOffer();
	Set<Offer> getAllActiveOffer();
	Offer getOfferById(int id);
	boolean assignOffer(int userId, int offerId);
	MyOffersDTO getMyOffers(int userId);
	
}
