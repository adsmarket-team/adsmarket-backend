package com.adsmarket.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adsmarket.constant.AppConstants;
import com.adsmarket.domain.Offer;
import com.adsmarket.domain.User;
import com.adsmarket.dto.MyOffersDTO;
import com.adsmarket.dto.OfferInfoDTO;
import com.adsmarket.repository.OfferRepository;
import com.adsmarket.repository.UserRepository;
import com.adsmarket.util.ConvertUtil;

@Service
@Transactional
public class OfferServiceImpl implements OfferService {
	
	@Autowired
	OfferRepository offerRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AdsCpiClickService adsCpiClickService;
	

	@Override
	public Set<Offer> getAllOffer() {
		return new HashSet<Offer>(offerRepository.findAll());
	}

	@Override
	public Set<Offer> getAllActiveOffer() {
		// TODO Auto-generated method stub
		return offerRepository.findByStatus(AppConstants.OFFER_STATUS_ACTIVE);
	}

	@Override
	public Offer getOfferById(int id) {
		// TODO Auto-generated method stub
		return offerRepository.findOne(id);
	}

	@Override
	public boolean assignOffer(int userId, int offerId) {
		try {
		User user = userRepository.findOne(userId);
		Set<Offer> offers = user.getOffers();
		offers.add(offerRepository.findOne(offerId));
		user.setOffers(offers);
		userRepository.saveAndFlush(user);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public MyOffersDTO getMyOffers(int userId) {
		// TODO Auto-generated method stub
		User user = userRepository.findOne(userId);
//		Set<Offer> userOffers = user.getOffers();
		List<Offer> myOffers = new ArrayList<Offer>(user.getOffers());
		List<OfferInfoDTO> offerInfoDTOs = new ArrayList<OfferInfoDTO>();
		OfferInfoDTO offerInfoDTO = null;
		for (Offer offer : myOffers) {
			offerInfoDTO = new OfferInfoDTO();
			offerInfoDTO.setTotalClick(adsCpiClickService.totalCountClick(user.getId(), offer.getId()));
			offerInfoDTO.setId(offer.getId());
			offerInfoDTO.setImageUrl(offer.getImageUrl());
			offerInfoDTO.setName(offer.getName());
			offerInfoDTO.setRegion(offer.getRegion());
			offerInfoDTO.setStatus(offer.getStatus());
			offerInfoDTO.setTotalCr(0);
			offerInfoDTO.setTotalEarning(0);
			offerInfoDTO.setTotalInstall(0);
			offerInfoDTO.setType(offer.getType());
			offerInfoDTO.setDescription(offer.getDescription());
//			offerInfoDTO.setPlatform(offer.getPlatform());
			offerInfoDTOs.add(offerInfoDTO);
		}
		MyOffersDTO myOffersDTO = new MyOffersDTO();
		myOffersDTO.setOfferInfoDTOs(offerInfoDTOs);;
		myOffersDTO.setTotalCount(Integer.toUnsignedLong(myOffers.size()));
		return myOffersDTO;
	}

}
