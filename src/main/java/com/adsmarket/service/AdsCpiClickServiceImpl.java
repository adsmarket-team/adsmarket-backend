package com.adsmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adsmarket.repository.AdsCpiClickRepository;

@Service
@Transactional
public class AdsCpiClickServiceImpl implements AdsCpiClickService  {

	@Autowired 
	AdsCpiClickRepository adsCpiClickRepository;
	
	@Override
	public int totalCountClick(int userId, int offerId) {
		
		return adsCpiClickRepository.findDistinctByUserIdAndOfferId(userId, offerId).size();
	}

}
