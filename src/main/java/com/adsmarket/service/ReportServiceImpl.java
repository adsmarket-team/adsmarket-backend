package com.adsmarket.service;

import com.adsmarket.domain.AdsCpiClick;
import com.adsmarket.domain.AdsCpiInstall;
import com.adsmarket.domain.User;
import com.adsmarket.repository.AdsCpiClickRepository;
import com.adsmarket.repository.AdsCpiInstallRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by quanh on 5/25/2017.
 */
@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    AdsCpiClickRepository adsCpiClickRepository;
    
    @Autowired
    AdsCpiInstallRepository adsCpiInstallRepository;


    @Autowired
    UserService userService;



    @Override
    public Long getTotalMonthlyClick() {
        User currentUser = userService.getLoggedInUser();
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer month = localDate.getMonthValue();
        Set<AdsCpiClick> totalClickThisMonth = adsCpiClickRepository.getUserTotalClickInSpecificMonth(currentUser.getId(), month);

        return (long) totalClickThisMonth.size();
    }

    @Override
    public Long getTotalMonthlyInstall() {
    	 User currentUser = userService.getLoggedInUser();
    	 Date date = new Date();
         LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
         Integer month = localDate.getMonthValue();
         Set<AdsCpiInstall> totalInstallThisMonth = adsCpiInstallRepository.getUserTotalInstallInSpecificMonth(currentUser.getId(), month);
        return (long) totalInstallThisMonth.size();
    }

    @Override
    public Long getTotalMonthlyConversion() {
        return null;
    }

	@Override
	public Long getTotalDailyClick() {
		// TODO Auto-generated method stub
//		User currentUser = userService.getLoggedInUser();
//		Date date = new Date();
//		String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
//		Set<AdsCpiClick> totalDailyClick = adsCpiClickRepository.getUserTotalClickInSpecificDate(currentUser.getId(), currentDate);
		
		return null;
	}

	@Override
	public Long getTotalDailyInstall() {
		// TODO Auto-generated method stub
		return null;
	}


}
