package com.adsmarket.service;

import org.springframework.security.core.userdetails.UserDetails;

import com.adsmarket.domain.User;

public interface UserService {
	
    void saveAndFlush(User user);
    User findByUserName(String username);
    User findByEmail(String email);
    User findById(Integer id);
    User getLoggedInUser();
}
