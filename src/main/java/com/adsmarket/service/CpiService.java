package com.adsmarket.service;

public interface CpiService {

	boolean userValidation(int userId);
	boolean offerValidation(int offerId, int userId);
	boolean recordClick(int offerId, int userId, String ip);
	boolean checkOfferRecordedClickIp(int userId, int offerId, String ip);
	String redirectOffer(Integer offerId, Integer userId);
	boolean postbackValidation(String accId, String vendorAppId, Integer vendorId, Integer userId, Integer offerId,
				String ip, String deviceId, String deviceName, String transactionId);
	void recordInstall(Integer userId, Integer offerId, String ip, String deviceId, String deviceName,
			String earning, String transactionId);

}
