package com.adsmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adsmarket.domain.UserBankInfo;
import com.adsmarket.domain.User;
import com.adsmarket.domain.UserInfo;
import com.adsmarket.repository.UserBankInfoRepository;
import com.adsmarket.repository.UserInfoRepository;
import com.adsmarket.repository.UserRepository;

@Service
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	UserInfoRepository userInfoRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserBankInfoRepository bankInfoRepository;
	
	
	
	
	@Override
	public UserInfo getUserDesc(Integer userId) {
		UserInfo userInfo = userInfoRepository.findById(userId);
		if (userInfo != null) {
			return userInfo;
		}
		else {
			return null;
		}
	}

	@Override
	public boolean updateUserDesc(UserInfo userInfo, User user) {
		try {
		user.setAccountType(userInfo.getAccountType());
		user.setAddress(userInfo.getAddress());
		user.setBirthday(userInfo.getBirthday());
		user.setDateOfIssue(userInfo.getDateOfIssue());
		user.setEmail(userInfo.getEmail());
		user.setFullName(userInfo.getFullName());
		user.setGender(userInfo.getGender());
		user.setIdentityId(userInfo.getIdentityId());
		user.setPhone(userInfo.getPhone());
		user.setPhone1(userInfo.getPhone1());
		user.setPlaceOfIssue(userInfo.getPlaceOfIssue());
		user.setSkype(userInfo.getSkype());
		user.setUsername(userInfo.getUsername());
		userRepository.saveAndFlush(user);
		} catch (Exception e) {
			return false;
		}
		return true;
		
	}

	@Override
	public UserBankInfo getUserBankInfo(Integer userId) {
		// TODO Auto-generated method stub
		User user = userRepository.findOne(userId);
		return user.getBankInfo();
	}

	@Override
	public boolean updateUserBankInfo(UserBankInfo bankInfo) {
		// TODO Auto-generated method stub
		User user = userRepository.findOne(bankInfo.getUserId());
		UserBankInfo currentBankInfo = user.getBankInfo();
		if (currentBankInfo != null) {
			currentBankInfo.setBankAccName(bankInfo.getBankAccName());
			currentBankInfo.setBankAccNo(bankInfo.getBankAccNo());
			currentBankInfo.setBankName(bankInfo.getBankName());
			currentBankInfo.setBankBranch(bankInfo.getBankBranch());
			currentBankInfo.setBankCountry(bankInfo.getBankCountry());
			currentBankInfo.setSwiftCode(bankInfo.getSwiftCode());
		} else {
			currentBankInfo = bankInfo;
		}
		try {
			bankInfoRepository.saveAndFlush(currentBankInfo);
		} catch (Exception e) {
			return false;
		}
	
		
		return true;
	}

}
