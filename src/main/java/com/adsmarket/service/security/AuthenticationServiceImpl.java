package com.adsmarket.service.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adsmarket.constant.AppConstants;
import com.adsmarket.domain.Role;
import com.adsmarket.domain.User;
import com.adsmarket.dto.RegistrationFormDTO;
import com.adsmarket.repository.RoleRepository;
import com.adsmarket.repository.UserRepository;
import com.adsmarket.service.UserService;

@Service
@Transactional
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleRepository roleRepository;
	
	
	
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


	public boolean authenticate(User user, String password) {
		if (bCryptPasswordEncoder.matches(password, user.getPassword())) {
			return true;
		}
		return false;
	}


	public User register(RegistrationFormDTO registerForm) {
		User newUser = new User();
		newUser.setFullName(registerForm.getFullname());
		newUser.setEmail(registerForm.getEmail());
		newUser.setAddress(registerForm.getAddress());
		newUser.setAccountType(registerForm.getAccountType());
		newUser.setUsername(registerForm.getUsername());
		newUser.setPassword(registerForm.getPassword());
		newUser.setRefereeId(registerForm.getReferee());
		newUser.setGender(registerForm.getGender());
		newUser.setStatus(AppConstants.NOT_ACTIVATED);
		
		// Set User Role
		String role = registerForm.getAccountType();
		Set<Role> roles = new HashSet<Role>();
		if(role.equals(AppConstants.ROLE_PUBLISHER)) {
			roles.add(roleRepository.findByName(AppConstants.ROLE_PUBLISHER));
		}
		else {
			roles.add(roleRepository.findByName(AppConstants.ROLE_MARKETER));
		}
		newUser.setRoles(roles);
		
		userService.saveAndFlush(newUser);
		return newUser;
		
	}

	public boolean checkReferee(String refereeUserName) {
		User user = userService.findByUserName(refereeUserName);
		if (user == null){
			return false;
		}
		else {
			return true;
		}
		
	}

}
