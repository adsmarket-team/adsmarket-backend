package com.adsmarket.service.security;

import com.adsmarket.domain.User;
import com.adsmarket.dto.RegistrationFormDTO;

/**
 * Authentication Service
 * @author Ha Quang Anh
 *
 */

public interface AuthenticationService {
	
	/**
	 * Authenticate
	 * @param userid
	 * @param password
	 * @return true or false
	 */

	
	boolean authenticate(User user, String password);
	User register(RegistrationFormDTO registerForm);
	boolean checkReferee(String refereeUserName);
}
