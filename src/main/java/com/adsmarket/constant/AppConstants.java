package com.adsmarket.constant;

public class AppConstants {
	//Role name
	public static String ROLE_ADMIN = "admin";
	public static String ROLE_PUBLISHER = "publisher";
	public static String ROLE_MARKETER = "marketer";
	public static String ROLE_MODERATOR = "moderator";
	//User Status
	public static String BANNED = "banned";
	public static String NOT_ACTIVATED = "unactivated";
	public static String ACTIVATED = "activated";
	
	//Offer Status
	public static String ACTIVE = "active";
	public static String INACTIVE = "inactive";
	
	//Vendor by ID
	public static int ADSOTA = 1;
	
	//PageName
	public static String VIEW_HOME = "home";
	public static String VIEW_CAMPAIGN_ALL = "campaign-all";
	public static String VIEW_PROFILE = "profile";
	public static String SUBVIEW_PROFILE_ATAVAR = "avatar";
	public static String SUBVIEW_PROFILE_PASSWORD = "password";
	public static String SUBVIEW_PROFILE_PERSONAL = "personal";
	public static String SUBVIEW_PROFILE_PAYMENT = "payment";
	public static String SUBVIEW_PROFILE_PRIVACY = "privacy";

	
	
	public static final String OFFER_STATUS_ACTIVE = "active";
}
