package com.adsmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adsmarket.domain.UserInfo;

public interface UserInfoRepository extends JpaRepository<UserInfo, Integer> {
	UserInfo findByUsername(String username);
	UserInfo findById(Integer userId);
}
