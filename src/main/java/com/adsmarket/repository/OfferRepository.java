package com.adsmarket.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adsmarket.domain.Offer;


public interface OfferRepository extends JpaRepository<Offer, Integer>  {
	
	Set<Offer> findByStatus(String status);
	Offer findByIdAndVendorAppIdAndVendorId(Integer id, String vendorAppId, Integer vendorId);
}
