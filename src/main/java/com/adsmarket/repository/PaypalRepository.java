package com.adsmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adsmarket.domain.Paypal;


public interface PaypalRepository extends JpaRepository<Paypal, Integer> {
	
}
