package com.adsmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adsmarket.domain.UserBankInfo;

public interface UserBankInfoRepository extends JpaRepository<UserBankInfo, Integer> {
	
}
