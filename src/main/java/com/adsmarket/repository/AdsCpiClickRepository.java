package com.adsmarket.repository;

import java.util.Date;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adsmarket.domain.AdsCpiClick;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdsCpiClickRepository extends JpaRepository<AdsCpiClick, Integer> {
	@Query("SELECT a FROM AdsCpiClick a where a.userId = :userId and function('MONTH', a.timestamp) = :month")
	Set<AdsCpiClick> getUserTotalClickInSpecificMonth(@Param("userId") Integer userId, @Param("month") Integer month);
	
	@Query("SELECT a FROM AdsCpiClick a where a.userId = :userId and cast(a.timestamp as date) = :date")
	Set<AdsCpiClick> getUserTotalClickInSpecificDate(@Param("userId") Integer userId, @Param("date") Date date);
	
	Set<AdsCpiClick> findDistinctByUserId(int userId);
	Set<AdsCpiClick> findDistinctByUserIdAndOfferId(int userId, int offerId);
	Set<AdsCpiClick> findDistinctByUserIdAndOfferIdAndIp(int userId, int offerId, String ip);


	
}
