package com.adsmarket.repository;

import java.util.Date;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adsmarket.domain.AdsCpiInstall;


public interface AdsCpiInstallRepository extends JpaRepository<AdsCpiInstall, Integer> {
//	Set<AdsCpiInstall> findDistinctByUserIdAndOfferIdAndIp(Integer userId, Integer offerId, String ip);
//	Set<AdsCpiInstall> findByUserIdAndOfferIdAndDeviceId(Integer userId, Integer offerId,String deviceId);
//	Set<AdsCpiInstall> findByUserIdAndOfferIdAndDeviceName(Integer userId, Integer offerId,String deviceName);
	@Query("SELECT a FROM AdsCpiInstall a where a.userId = :userId and function('MONTH', a.timestamp) = :month")
	Set<AdsCpiInstall> getUserTotalInstallInSpecificMonth(@Param("userId") Integer userId, @Param("month") Integer month);
	
	@Query("SELECT a FROM AdsCpiInstall a where a.userId = :userId and cast(a.timestamp as date) = :date")
	Set<AdsCpiInstall> getUserTotalInstallInSpecificDate(@Param("userId") Integer userId, @Param("date") String date);
	

	Set<AdsCpiInstall> findDistinceByUserId(@Param("userId") Integer userId);
	
	
	Set<AdsCpiInstall> findDistinctByUserIdAndOfferIdAndDeviceNameOrDeviceIdOrIp(Integer userId, Integer offerId, String deviceName, String deviceId, String ip);
}
