package com.adsmarket.dto;

public class GlobalPageDTO {
	private String viewName;
	private String subView;

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getSubView() {
		return subView;
	}

	public void setSubView(String subView) {
		this.subView = subView;
	}
	
	
}
