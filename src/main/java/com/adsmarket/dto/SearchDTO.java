package com.adsmarket.dto;


import org.hibernate.validator.constraints.NotEmpty;

public class SearchDTO {
	
	@NotEmpty
	private String name;
	@NotEmpty 
	private String type;
	private String incentType;
	@NotEmpty
	private String platform;
	@NotEmpty
	private String country;
	@NotEmpty
	private String status;
	@NotEmpty
	private String minPayout;
	@NotEmpty
	private String maxPayout;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIncentType() {
		return incentType;
	}
	public void setIncentType(String incentType) {
		this.incentType = incentType;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMinPayout() {
		return minPayout;
	}
	public void setMinPayout(String minPayout) {
		this.minPayout = minPayout;
	}
	public String getMaxPayout() {
		return maxPayout;
	}
	public void setMaxPayout(String maxPayout) {
		this.maxPayout = maxPayout;
	}
	
	

}
