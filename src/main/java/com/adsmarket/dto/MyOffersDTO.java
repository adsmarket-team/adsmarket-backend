package com.adsmarket.dto;

import java.util.List;

import com.adsmarket.domain.Offer;

public class MyOffersDTO {
	private Long totalCount;
	private List<OfferInfoDTO> offerInfoDTOs;
	public Long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	public List<OfferInfoDTO> getOfferInfoDTOs() {
		return offerInfoDTOs;
	}
	public void setOfferInfoDTOs(List<OfferInfoDTO> offerInfoDTOs) {
		this.offerInfoDTOs = offerInfoDTOs;
	}

	
}
