package com.adsmarket.dto;

public class OfferDTO {
	private int id;
	private String name;
	private String imageUrl;
	private String type;
	private String region;
	private String status;
	private String platform;
	private Long payout;
	private String incentType;
	private String requirement;
	private String trackingLink;
	private String cap;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public Long getPayout() {
		return payout;
	}
	public void setPayout(Long payout) {
		this.payout = payout;
	}
	public String getIncentType() {
		return incentType;
	}
	public void setIncentType(String incentType) {
		this.incentType = incentType;
	}
	public String getRequirement() {
		return requirement;
	}
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}
	public String getTrackingLink() {
		return trackingLink;
	}
	public void setTrackingLink(String trackingLink) {
		this.trackingLink = trackingLink;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	
	
}
