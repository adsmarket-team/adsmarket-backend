package com.adsmarket.dto;

public class OfferInfoDTO {
	
	private int id;
	private String name;
	private String imageUrl;
	private String type;
	private String region;
	private String status;
	private String description;
	private Integer platform;
	private int totalClick;
	private int totalInstall;
	private int totalCr;
	private int totalEarning;
	
	
	public OfferInfoDTO() {
		super();
	}
	
	public Integer getPlatform() {
		return platform;
	}

	public void setPlatform(Integer platform) {
		this.platform = platform;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTotalClick() {
		return totalClick;
	}
	public void setTotalClick(int totalClick) {
		this.totalClick = totalClick;
	}
	public int getTotalInstall() {
		return totalInstall;
	}
	public void setTotalInstall(int totalInstall) {
		this.totalInstall = totalInstall;
	}

	public int getTotalCr() {
		return totalCr;
	}

	public void setTotalCr(int totalCr) {
		this.totalCr = totalCr;
	}

	public int getTotalEarning() {
		return totalEarning;
	}

	public void setTotalEarning(int totalEarning) {
		this.totalEarning = totalEarning;
	}



	
	

}
