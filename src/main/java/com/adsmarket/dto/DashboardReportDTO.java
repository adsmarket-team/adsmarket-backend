package com.adsmarket.dto;

import java.util.Date;

/**
 * Created by quanh on 5/24/2017.
 */ 
public class DashboardReportDTO implements Comparable<DashboardReportDTO>{
    private Long monthlyTotalClick;

    private Long monthlyTotalInstall;
    
    private Long monthlyTotalConversion;
    
    private Long actualPayout;
    private Long targetPayout;
    private Long totalClicks;
    private Long totalInstall;
    private Long totalTransaction;
   

    private Long dailyTotalClick;

    private Long dailyTotalInstall;

    private Long dailyTotalConversion;

    private Date date;
    
    private String dateString;
    
    
    
    
    public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getDailyTotalClick() {
		return dailyTotalClick;
	}

	public void setDailyTotalClick(Long dailyTotalClick) {
		this.dailyTotalClick = dailyTotalClick;
	}

	public Long getDailyTotalInstall() {
		return dailyTotalInstall;
	}

	public void setDailyTotalInstall(Long dailyTotalInstall) {
		this.dailyTotalInstall = dailyTotalInstall;
	}

	public Long getMonthlyTotalClick() {
        return monthlyTotalClick;
    }

    public void setMonthlyTotalClick(Long monthlyTotalClick) {
        this.monthlyTotalClick = monthlyTotalClick;
    }



    public Long getMonthlyTotalInstall() {
        return monthlyTotalInstall;
    }

    public void setMonthlyTotalInstall(Long monthlyTotalInstall) {
        this.monthlyTotalInstall = monthlyTotalInstall;
    }

    

    public Long getActualPayout() {
		return actualPayout;
	}

	public void setActualPayout(Long actualPayout) {
		this.actualPayout = actualPayout;
	}

	public Long getTargetPayout() {
		return targetPayout;
	}

	public void setTargetPayout(Long targetPayout) {
		this.targetPayout = targetPayout;
	}

	public Long getTotalClicks() {
        return totalClicks;
    }

    public void setTotalClicks(Long totalClicks) {
        this.totalClicks = totalClicks;
    }

    public Long getTotalInstall() {
        return totalInstall;
    }

    public void setTotalInstall(Long totalInstall) {
        this.totalInstall = totalInstall;
    }

    public Long getTotalTransaction() {
        return totalTransaction;
    }

    public void setTotalTransaction(Long totalTransaction) {
        this.totalTransaction = totalTransaction;
    }

    public Long getMonthlyTotalConversion() {
        return monthlyTotalConversion;
    }

    public void setMonthlyTotalConversion(Long monthlyTotalConversion) {
        this.monthlyTotalConversion = monthlyTotalConversion;
    }

    public Long getDailyTotalConversion() {
        return dailyTotalConversion;
    }

    public void setDailyTotalConversion(Long dailyTotalConversion) {
        this.dailyTotalConversion = dailyTotalConversion;
    }

	@Override
	public int compareTo(DashboardReportDTO o) {
		// TODO Auto-generated method stub
		if (getDate() == null || o.getDate() == null)
		      return 0;
		return getDate().compareTo(o.getDate());
	}
}
