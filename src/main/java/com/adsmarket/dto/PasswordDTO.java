package com.adsmarket.dto;

public class PasswordDTO {
	private String oldPassword;
	private String newPassword;
	private String rNewPassword;
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getrNewPassword() {
		return rNewPassword;
	}
	public void setrNewPassword(String rNewPassword) {
		this.rNewPassword = rNewPassword;
	}
	
	
}
