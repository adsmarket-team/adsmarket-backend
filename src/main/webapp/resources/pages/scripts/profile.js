var Profile = function() {

    var dashboardMainChart = null;

    return {

        //main function
        init: function() {
        
            Profile.initMiniCharts();
        },
        
        initMiniCharts: function() {

            // IE8 Fix: function.bind polyfill
            if (App.isIE8() && !Function.prototype.bind) {
                Function.prototype.bind = function(oThis) {
                    if (typeof this !== "function") {
                        // closest thing possible to the ECMAScript 5 internal IsCallable function
                        throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
                    }

                    var aArgs = Array.prototype.slice.call(arguments, 1),
                        fToBind = this,
                        fNOP = function() {},
                        fBound = function() {
                            return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
                                aArgs.concat(Array.prototype.slice.call(arguments)));
                        };

                    fNOP.prototype = this.prototype;
                    fBound.prototype = new fNOP();

                    return fBound;
                };
            }

            $("#sparkline_bar").sparkline([8, 9, 10, 11, 10, 10, 12, 10, 10, 11, 9, 12, 11], {
                type: 'bar',
                width: '100',
                barWidth: 6,
                height: '45',
                barColor: '#F36A5B',
                negBarColor: '#e02222'
            });

            $("#sparkline_bar2").sparkline([9, 11, 12, 13, 12, 13, 10, 14, 13, 11, 11, 12, 11], {
                type: 'bar',
                width: '100',
                barWidth: 6,
                height: '45',
                barColor: '#5C9BD1',
                negBarColor: '#e02222'
            });
        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        Profile.init();
//        $(document).on("submit","#changeAvatar", function(e) {
//    
//
//           
//        });
    });
}

function updateImage() {

	$('#changeAvatar').unbind('submit').bind('submit', function(e) {
		e.preventDefault();
        var formData = new FormData(this);
        console.log(formData);
        $.ajax({
        type: 'POST',
        url: "/profile/changeAvatar",
        data:formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
        success: function(data) {
        	if (data == true) {
				$
						.alert({
							title : 'Successfully updated',
							type : 'green',
							closeIcon : true,
							closeIconClass : 'fa fa-close',
							columnClass : 'medium',
							typeAnimated : true,
							content : 'Your profile picture has been successfully updated !',
						});
			} else {
				$
						.alert({
							title : 'Something wrong!',
							type : 'red',
							closeIcon : true,
							closeIconClass : 'fa fa-close',
							columnClass : 'medium',
							typeAnimated : true,
							content : 'Somthing wrong while updating your profile picture, please try again!',
						});
			}
        },
        error: function(xhr, status, error) {
        	$
			.alert({
				title : 'Something wrong!',
				type : 'red',
				closeIcon : true,
				closeIconClass : 'fa fa-close',
				columnClass : 'medium',
				typeAnimated : true,
				content : 'Somthing wrong while updating your profile picture, please try again or contact admin!',
			});
        	}
        });
      
        return true;
	});

}