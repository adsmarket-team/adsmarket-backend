(function($, window, document, undefined) {
    'use strict';

    // init cubeportfolio
    $('#js-grid-juicy-projects').cubeportfolio({
        filters: '#js-filters-juicy-projects',
        loadMore: '#js-loadMore-juicy-projects',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        defaultFilter: '*',
        animationType: 'quicksand',
        gapHorizontal: 10,
        gapVertical: 20,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 8
        }, {
            width: 1100,
            cols: 4
        }, {
            width: 800,
            cols: 3
        }, {
            width: 480,
            cols: 2
        }, {
            width: 320,
            cols: 1
        }],
        caption: 'overlayBottomReveal',
        displayType: 'sequentially',
        displayTypeSpeed: 80,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 10000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
        },
    });
    
})(jQuery, window, document);

function getOfferDetails(ele) {

	$.ajax({
		type : 'POST',
		url : '/campaign/getOfferDetail',
		data : { offerId : $(ele).attr('id')},
		dataType : 'json',
		encode : true,
		success : function(data) {

			$('.offer-detail-name').text(data.name);
			$('.offer-detail-img').attr("src",data.imageUrl);
			$('.offer-detail-incent_type').text(data.incentType);
			$('.offer-detail-payout').text(data.payout);
			$('.offer-detail-type').text(data.incentType);
			$('.offer-detail-region').text(data.region);
			$('.offer-detail-platform').text(data.platform);
			$('.offer-detail-cap').text(data.cap);
			$('.offer-detail-requirement').text(data.requirement);
			$('.offer-detail-trackingLink').val(data.trackingLink);

			return true;
		},
	    error: function(xhr, textStatus, errorThrown){
	        alert(errorThrown);
	     }
	});
}
function search(ele) {
	var name = $('#name').val();
	var type = $('#type').val();
	var incentType = $('#incentType').val();
	var platform = $('#platform').val();
	var country = $('#country').val();
	var status = $('#status').val();
	var minPayout = parseInt($('.irs-from').text().replace(/ /g,''));
	var maxPayout = parseInt($('.irs-to').text().replace(/ /g,''));
	$.each($('.cbp-item'),function(){
		$(this).removeClass('satisfy');
	});
	
	$.each($('.cbp-item'),function(){
		var classList = $(this).attr('class').split(/\s+/);
		var itemName = $(this).find('.item-name').text();
		var itemRegion = $(this).find('.item-country').text();
		var condition1 = itemName.indexOf(name) >= 0;
		var condition2 =  type == classList[4];
		var condition3 = incentType == classList[2];
		var condition4 = platform == classList[1];	
		var condition5 = itemRegion.indexOf(country)>= 0;
		var condition6 = status == classList[3];
		var condition7 = parseInt(classList[5]) >=  minPayout ;
		var condition8 = parseInt(classList[5]) <=  maxPayout ;
		if (condition1 && condition2 && condition3 && condition4 && condition5 && condition6 && condition7 && condition8) {
			$(this).addClass('satisfy');
		}
		
	});
	$('.noneTrigger').bind('click', function() {
		setTimeout(function(){ $('.searchTrigger').trigger('click'); }, 700);
		
	});
	
	$('.noneTrigger').trigger( "click" );

	
}