var FormValidationMd = function() {

	var handlePaymentMethod = function() {
		console.log($('.bank').val());
		if($('.bank').val() == 1) {
			$('.bank').val(0);
			$('.bank').prop('checked', true);
			$('.bank').change();

		}
		if($('.paypal').val() == 1) {
			$('.paypal').val(0);
			$('.paypal').prop('checked', true);
			$('.paypal').change();
		}
		

		

	}
	var handleValidation1 = function() {
		// for more info visit the official plugin documentation:
		// http://docs.jquery.com/Plugins/Validation
		var form1 = $('#userBankInfo');
		var error1 = $('.alert-danger', form1);
		var success1 = $('.alert-success', form1);

		form1
				.validate({
					errorElement : 'span', // default input error message
					// container
					errorClass : 'help-block help-block-error', // default input
					// error message
					// class
					focusInvalid : false, // do not focus the last invalid
					// input
					ignore : "", // validate all fields including form hidden
					// input
					rules : {
						bankName : {
							minlength : 2,
							required : true
						},
						bankCountry : {
							required : true
						},
						bankAccName : {
							required : true
						},
						bankAccNo : {
							required : true
						}

					},

					invalidHandler : function(event, validator) { // display
						// error
						// alert on
						// form
						// submit
						success1.hide();
						error1.show();
						App.scrollTo(error1, -200);
					},

					errorPlacement : function(error, element) {

						error.insertAfter(element); // for other inputs, just
						// perform default behavior

					},

					highlight : function(element) { // hightlight error inputs
						$(element).closest('.form-group').addClass('has-error'); // set
						// error
						// class
						// to
						// the
						// control
						// group
					},

					unhighlight : function(element) { // revert the change
						// done by hightlight
						$(element).closest('.form-group').removeClass(
								'has-error'); // set error class to the
						// control group
					},

					success : function(label) {
						label.closest('.form-group').removeClass('has-error'); // set
						// success
						// class
						// to
						// the
						// control
						// group
					},

					submitHandler : function(form) {
						success1.show();
						error1.hide();
					

					}
				});
	}
	var handleValidation2 = function() {
		// for more info visit the official plugin documentation:
		// http://docs.jquery.com/Plugins/Validation
		var form1 = $('#paypalDTO');
		var error1 = $('.alert-danger', form1);
		var success1 = $('.alert-success', form1);

		form1
				.validate({
					errorElement : 'span', // default input error message
					// container
					errorClass : 'help-block help-block-error', // default input
					// error message
					// class
					focusInvalid : false, // do not focus the last invalid
					// input
					ignore : "", // validate all fields including form hidden
					// input
					rules : {
						paypal : {
							required : true
						}

					},

					invalidHandler : function(event, vonchangealidator) { // display

						success1.hide();
						error1.show();
						App.scrollTo(error1);
					},

					errorPlacement : function(error, element) {

						error.insertAfter(element); // for other inputs, just
						// perform default behavior

					},

					highlight : function(element) { // hightlight error inputs
						$(element).closest('.form-group').addClass('has-error'); // set

					},

					unhighlight : function(element) { // revert the change
						// done by hightlight
						$(element).closest('.form-group').removeClass(
								'has-error'); // set error class to the
						// control group
					},

					success : function(label) {
						label.closest('.form-group').removeClass('has-error'); // set

					},

					submitHandler : function(form) {
						success1.show();
						error1.hide();

						// process the form
//						var token = $("meta[name='_csrf']").attr("content");
//						var header = $("meta[name='_csrf_header']").attr(
//								"content");
//
//						$(document).ajaxSend(function(e, xhr, options) {
//							xhr.setRequestHeader(header, token);
//						});

					

					}
				});
	}
	return {
		// main function to initiate the module
		init : function() {
			handlePaymentMethod();
			handleValidation1();
			handleValidation2();
		}
	};
}();


	
	

function validateBankInfo() {
		if($("#userBankInfo").valid()) {
			var formData = {
					'bankName' : $('#bankName')
							.val(),
					'bankCountry' : $(
							'#bankCountry').val(),
					'bankAccName' : $(
							'#bankAccName').val(),
					'bankAccNo' : $('#bankAccNo')
							.val(),
					'swiftCode' : $('#swiftCode')
							.val(),
					'bankBranch' : $('#bankBranch')
							.val()
				};
			$.ajax({
				type : 'POST',
				url : '/profile/payment/bankUpdate',
				data : JSON.stringify(formData),
				contentType : "application/json",
				dataType : 'json',
				encode : true,
				success : function(data) {
					
					if (data == true) {
						$
								.alert({
									title : 'Successfully updated',
									type : 'green',
									closeIcon : true,
									closeIconClass : 'fa fa-close',
									columnClass : 'medium',
									typeAnimated : true,
									content : 'Your bank info has been successfully updated !',
								});
					} else {
						$
								.alert({
									title : 'Something wrong!',
									type : 'red',
									closeIcon : true,
									closeIconClass : 'fa fa-close',
									columnClass : 'medium',
									typeAnimated : true,
									content : 'Somthing wrong while updating your bank info, please re-submit again!',
								});
					}
				},
			    error: function(xhr, textStatus, errorThrown){
			        alert(errorThrown);
			     }
			});
		}

}
function validatePaypal() {
	if($("#paypalDTO").valid()) {
		var formData = {
				'paypal' : $('#paypal')
						.val()
			};
		$.ajax({
			type : 'POST',
			url : '/profile/payment/paypalUpdate',
			data : JSON.stringify(formData),
			contentType : "application/json",
			dataType : 'json',
			encode : true,
			success : function(data) {
				
				if (data == true) {
					$
							.alert({
								title : 'Successfully updated',
								type : 'green',
								closeIcon : true,
								closeIconClass : 'fa fa-close',
								columnClass : 'medium',
								typeAnimated : true,
								content : 'Your paypal has been successfully updated !',
							});
				} else {
					$
							.alert({
								title : 'Something wrong!',
								type : 'red',
								closeIcon : true,
								closeIconClass : 'fa fa-close',
								columnClass : 'medium',
								typeAnimated : true,
								content : 'Somthing wrong while updating your paypal, please re-submit again!',
							});
				}
			},
		    error: function(xhr, textStatus, errorThrown){
		        alert(errorThrown);
		     }
		});
	}

}
jQuery(document).ready(function() {

	FormValidationMd.init();

	
});

function changeEnable(ele) {
	if($(ele).val() == 0) {
		$(ele).val(1);
		if ($(ele).attr('id') == "bank-info") {
			$('.bank-content').addClass('active');
		} else {
			$('.paypal-content').addClass('active');
		}
	}
	else {
		$(ele).val(0);
		if ($(ele).attr('id') == "bank-info") {
			$('.bank-content').removeClass('active');
		} else {
			$('.paypal-content').removeClass('active');
		}
	}
}