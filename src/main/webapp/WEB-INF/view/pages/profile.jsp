<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.adsmarket.constant.AppConstants"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<div class="page-content">
	<!-- BEGIN BREADCRUMBS -->
	<div class="breadcrumbs">
		<h1>User Profile | Account</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Profile</li>
		</ol>
		<!-- Sidebar Toggle Button -->
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".page-sidebar">
			<span class="sr-only">Toggle navigation</span> <span
				class="toggle-icon"> <span class="icon-bar"></span> <span
				class="icon-bar"></span> <span class="icon-bar"></span>
			</span>
		</button>
		<!-- Sidebar Toggle Button -->
	</div>
	<!-- END BREADCRUMBS -->
	<!-- BEGIN SIDEBAR CONTENT LAYOUT -->
	<div class="page-content-container">
		<div class="page-content-row">
			<div class="page-content-col">
				<!-- BEGIN PAGE BASE CONTENT -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN PROFILE SIDEBAR -->
						<div class="profile-sidebar">
							<!-- PORTLET MAIN -->
							<div class="portlet light profile-sidebar-portlet bordered">
								<!-- SIDEBAR USERPIC -->
								
								<div class="profile-userpic">
									<img src="pages/media/profile/profile_user.jpg"
										class="img-responsive" alt="">
								</div>
								<!-- END SIDEBAR USERPIC -->
								<!-- SIDEBAR USER TITLE -->
								<div class="profile-usertitle">
									<div class="profile-usertitle-name">${userInfo.fullName}</div>
									<div class="profile-usertitle-job">${userInfo.accountType}</div>
								</div>
								<!-- END SIDEBAR USER TITLE -->
								<!-- SIDEBAR BUTTONS -->
								<div class="profile-userbuttons">
									<button type="button" class="btn btn-circle green btn-sm">Upload</button>
									<button type="button" class="btn btn-circle red btn-sm">Change</button>
								</div>
								<!-- END SIDEBAR BUTTONS -->
								<!-- SIDEBAR MENU -->
								<div class="profile-usermenu">
									<ul class="nav">
										<li><a href="page_user_profile_1.html"> <i
												class="icon-home"></i> Overview
										</a></li>
										<li class="active"><a
											href="page_user_profile_1_account.html"> <i
												class="icon-settings"></i> Account Settings
										</a></li>
										<li><a href="page_user_profile_1_help.html"> <i
												class="icon-info"></i> Help
										</a></li>
									</ul>
								</div>
								<!-- END MENU -->
							</div>
							<!-- END PORTLET MAIN -->
							<!-- PORTLET MAIN -->
							<div class="portlet light bordered">
								<!-- STAT -->
								<div class="row list-separated profile-stat">
									<div class="col-md-4 col-sm-4 col-xs-6">
										<div class="uppercase profile-stat-title">37</div>
										<div class="uppercase profile-stat-text">Projects</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-6">
										<div class="uppercase profile-stat-title">51</div>
										<div class="uppercase profile-stat-text">Tasks</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-6">
										<div class="uppercase profile-stat-title">61</div>
										<div class="uppercase profile-stat-text">Uploads</div>
									</div>
								</div>
								<!-- END STAT -->
								<div>
									<h4 class="profile-desc-title">About Marcus Doe</h4>
									<span class="profile-desc-text"> Lorem ipsum dolor sit
										amet diam nonummy nibh dolore. </span>
									<div class="margin-top-20 profile-desc-link">
										<i class="fa fa-globe"></i> <a
											href="http://www.keenthemes.com">www.keenthemes.com</a>
									</div>
									<div class="margin-top-20 profile-desc-link">
										<i class="fa fa-twitter"></i> <a
											href="http://www.twitter.com/keenthemes/">@keenthemes</a>
									</div>
									<div class="margin-top-20 profile-desc-link">
										<i class="fa fa-facebook"></i> <a
											href="http://www.facebook.com/keenthemes/">keenthemes</a>
									</div>
								</div>
							</div>
							<!-- END PORTLET MAIN -->
						</div>
						<!-- END BEGIN PROFILE SIDEBAR -->
						<!-- BEGIN PROFILE CONTENT -->
						<div class="profile-content">
							<div class="row">
								<div class="col-md-12">
									<div class="portlet light bordered">
										<div class="portlet-title tabbable-line">
											<div class="caption caption-md">
												<i class="icon-globe theme-font hide"></i> <span
													class="caption-subject font-blue-madison bold uppercase">Profile
													Account</span>
											</div>
											<ul class="nav nav-tabs">

												<li
													class="<c:if test="${subViewName == AppConstants.SUBVIEW_PROFILE_PERSONAL}">active</c:if>"><a
													href="#tab_1_1" data-toggle="tab">Personal Info</a></li>
												<li
													class="<c:if test="${subViewName == AppConstants.SUBVIEW_PROFILE_PAYMENT}">active</c:if>"><a
													href="#tab_1_2" data-toggle="tab">Payment </a></li>
												<li
													class="<c:if test="${subViewName == AppConstants.SUBVIEW_PROFILE_ATAVAR}">active</c:if>"><a
													href="#tab_1_3" data-toggle="tab">Change Avatar</a></li>
												<li
													class="<c:if test="${subViewName == AppConstants.SUBVIEW_PROFILE_PASSWORD}">active</c:if>"><a
													href="#tab_1_4" data-toggle="tab">Change Password</a></li>
												<li
													class="<c:if test="${subViewName == AppConstants.SUBVIEW_PROFILE_PRIVACY}">active</c:if>"><a
													href="#tab_1_5" data-toggle="tab">Privacy Settings</a></li>
											</ul>
										</div>
										<div class="portlet-body">
											<div class="tab-content">
												<!-- PERSONAL INFO TAB -->
												<div class="tab-pane active" id="tab_1_1">
													<form:form action="${contextPath}/profile/updateUserDesc"
														method="POST" modelAttribute="userInfo">
														<div class="form-body">
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control"
																	path="fullName"></form:input>
																<label for="fullName">Full Name</label>
															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label has-success">
																<form:input type="text" cssClass="form-control"
																	readonly="true" path="username"></form:input>
																<label for="username">Username</label>

															</div>

															<!-- 															<div
																class="form-group form-md-line-input form-md-floating-label">
																<input type="text" class="form-control" name="email"
																	id="form_control_1"> <label
																	for="form_control_1">Email<span class="required" aria-required="true">*</span></label> <span
																		class="input-group-addon"> <i
																		class="fa fa-envelope"></i>
																	</span><span
																	class="help-block">Please enter your correct email...</span>
															</div> -->
															<div
																class="form-group form-md-line-input form-md-floating-label has-success">
																<div class="input-group">
																	<form:input type="text" cssClass="form-control"
																		readonly="true" path="email"></form:input>
																	<label for="email">Email</label> <span
																		class="input-group-addon"> <i
																		class="fa fa-envelope"></i>
																	</span>
																</div>
															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<div class="input-group">
																	<form:input type="text" cssClass="form-control"
																		path="address"></form:input>
																	<label for="address">Address (Optional)</label> <span
																		class="input-group-addon"> <i
																		class="fa fa-envelope"></i>
																	</span>
																</div>
															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<div class="input-group">
																	<form:input type="text"
																		cssClass="form-control mask_date2" path="birthday"></form:input>
																	<label for="birthday">Birthday (Optional)</label> <span
																		class="input-group-addon"> <i
																		class="fa fa-envelope"></i>
																	</span>
																</div>
															</div>

															<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control"
																	path="phone"></form:input>
																<label for="phone">Phone Number 1</label>
															</div>

															<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control"
																	path="phone1"></form:input>
																<label for="phone1">Phone Number 2</label>
															</div>

															<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control"
																	path="skype"></form:input>
																<label for="skype">Skype ID</label>
															</div>


															<%-- 			<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control" path="facebook"
																	value="${userInfo.facebook}"></form:input> <label
																	for="facebook">Facebook ID</label>
															</div>
															
															
																<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control" path="im"
																	value="${userInfo.im}"></form:input> <label
																	for="im">Other Im</label>
															</div> --%>
															<div class="form-group form-md-radios">

																<label>Gender</label>
																<div class="md-radio-inline">
																	<div class="md-radio">
																		<form:radiobutton id="gender1" path="gender"
																			cssClass="md-radiobtn" checked="checked" value="male" />
																		<label for="gender1"> <span class="inc"></span>
																			<span class="check"></span> <span class="box"></span>Male
																		</label>
																	</div>
																	<div class="md-radio">
																		<form:radiobutton id="gender2" path="gender"
																			cssClass="md-radiobtn" value="female" />
																		<label for="gender2"> <span></span> <span
																			class="check"></span> <span class="box"></span>
																			Female
																		</label>
																	</div>

																</div>

															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control"
																	path="identityId"></form:input>
																<label for="identityId">Passport/CMND<span
																	class="required" aria-required="true">*</span></label>
															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<div class="input-group">
																	<form:input type="text"
																		cssClass="form-control mask_date2" path="dateOfIssue"></form:input>
																	<label for="dateOfIssue">Date Of Issue<span
																		class="required" aria-required="true">*</span></label> <span
																		class="input-group-addon"> <i
																		class="fa fa-envelope"></i>
																	</span>
																</div>
															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<form:input type="text" cssClass="form-control"
																	path="placeOfIssue"></form:input>
																<label for="placeOfIssue">Place Of Issue<span
																	class="required" aria-required="true">*</span></label>
															</div>

														</div>
														<div class="form-actions">
															<div class="row">
																<div class="col-md-12">
																	<button type="submit" class="btn dark">Update</button>
																	<button type="reset" class="btn default">Reset</button>
																</div>
															</div>
														</div>
													</form:form>
												</div>
												<!-- END PERSONAL INFO TAB -->
												<!-- PAYMENT TAB -->
												<div class="tab-pane active" id="tab_1_2">
													<div class="row">
														<div class="col-md-4">
															<div class="portlet light bordered">
																<div class="portlet-body">
																	<form:form method="POST" modelAttribute="userBankInfo">
																	<div class="row">
																		<div class="col-md-8">
																			<img src="/resources/custom/img/icons/bank.svg"
																				style="width: 100px; height: auto;" />
																		</div>
																		<div class="col-md-4 text-center">
																			<input type="checkbox" class="make-switch bank" id="bank-info"
																				data-on-color="success"
																				data-off-color="danger" value="${userBankInfo.enable}" onchange="changeEnable(this)">

																		</div>
																	</div>
																	<div class="row bank-content">

																	
																			<div class="form-body">
																				<div
																					class="form-group form-md-line-input form-md-floating-label">
																					<form:input type="text" cssClass="form-control"
																						path="bankName"></form:input>
																					<label for="bankName">Bank Name<span
																						class="required" aria-required="true">*</span></label>
																				</div>
																				<div
																					class="form-group form-md-line-input form-md-floating-label">
																					<form:input type="text" cssClass="form-control"
																						path="bankBranch"></form:input>
																					<label for="bankBranch">Bank Branch
																						(Optional)</label>
																				</div>
																				<div
																					class="form-group form-md-line-input form-md-floating-label">
																					<form:input type="text" cssClass="form-control"
																						path="bankCountry"></form:input>
																					<label for="bankCountry">Bank Country<span
																						class="required" aria-required="true">*</span></label>
																				</div>
																				<div
																					class="form-group form-md-line-input form-md-floating-label">
																					<form:input type="text" cssClass="form-control"
																						path="bankAccName"></form:input>
																					<label for="bankAccName">Account Owner Name<span
																						class="required" aria-required="true">*</span></label>
																				</div>
																				<div
																					class="form-group form-md-line-input form-md-floating-label">
																					<form:input type="text" cssClass="form-control"
																						path="bankAccNo"></form:input>
																					<label for="bankAccNo">Account Number<span
																						class="required" aria-required="true">*</span></label>
																				</div>
																				<div
																					class="form-group form-md-line-input form-md-floating-label">
																					<form:input type="text" cssClass="form-control"
																						path="swiftCode"></form:input>
																					<label for="swiftCode">SWIFT Code (If bank
																						is outsite of Vietnam)</label>
																				</div>



																			</div>
																			<div class="form-actions">
																				<div class="row">
																					<div class="col-md-12">
																						<button type="button" onclick="validateBankInfo()"
																							class="btn dark">Update</button>
																						<button type="reset" class="btn defa#ult">Clear</button>
																					</div>
																				</div>
																			</div>
																		
																	</div>
																	</form:form>


																</div>
															</div>
														</div>
														<div class="col-md-4">
															<div class="portlet light bordered">
																<div class="portlet-body">
																	<form:form
																	
																		method="POST" modelAttribute="paypalDTO">
																		<div class="row">
																			<div class="col-md-8">
																				<img src="/resources/custom/img/icons/paypal.svg"
																					style="width: 100px; height: auto;" />
																			</div>
																			<div class="col-md-4 text-center">
																				<input type="checkbox" class="make-switch paypal"
																					data-on-color="success"
																					data-off-color="danger" value="${paypalDTO.enable}" onchange="changeEnable(this)">

																			</div>
																		</div>
																		<div class="row paypal-content">

																			<div class="form-body">
																				<div
																					class="form-group form-md-line-input form-md-floating-label">
																					<form:input type="text" cssClass="form-control"
																						path="paypal"></form:input>
																					<label for="paypal">Paypal Id (Email)<span
																						class="required" aria-required="true">*</span></label>
																				</div>




																			</div>
																			<div class="form-actions">
																				<div class="row">
																					<div class="col-md-12">
																						<button type="button" onclick="validatePaypal()"
																							class="btn dark">Update</button>
																						<button type="reset" class="btn default">Clear</button>
																					</div>
																				</div>
																			</div>

																		</div>
																	</form:form>


																</div>
															</div>
														</div>
														<div class="col-md-4">
															<div class="portlet light bordered">
																<div class="portlet-body">
																	<form action="#" class="form-horizontal form-bordered">
																		<div class="form-body">
																			<div class="form-group">
																				<div class="col-md-10">
																					<img
																						src="/resources/custom/img/icons/credit-cards.svg"
																						style="width: 100px; height: auto;" />
																				</div>
																				<div class="col-md-2">
																					<input type="checkbox" class="make-switch bank"
																						data-on-color="success" data-off-color="danger">

																				</div>
																			</div>
																		</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- CHANGE AVATAR TAB -->
												<div class="tab-pane" id="tab_1_3">

												  <form action="#" enctype="multipart/form-data" id="changeAvatar" class="form-horizontal form-bordered" >
                                                    <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Profile Picture: </label>
                                                            <div class="col-md-9">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="profileImage" novalidate> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix margin-top-10">
                                                                    <span class="label label-danger">NOTE!</span> Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+ and Opera11.1+. In older browsers the filename is shown instead. </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button class="btn green" onclick="updateImage()">
                                                                    <i class="fa fa-check"></i> Submit</button>
                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
												</div>
												<!-- END CHANGE AVATAR TAB -->
												<!-- CHANGE PASSWORD TAB -->
												<div class="tab-pane" id="tab_1_4">
													<form:form action="${contextPath}/profile/updatePassword"
														id="change-password" modelAttribute="passwordDTO">
														<div class="form-body">
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<spring:bind path="oldPassword">
																	<form:input type="text" cssClass="form-control"
																		path="oldPassword"></form:input>
																	<label for="oldPassword">Current Password<span
																		class="required" aria-required="true">*</span></label>
																	<form:errors cssClass="help-block" path="oldPassword"></form:errors>
																</spring:bind>
															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<spring:bind path="newPassword">
																	<form:input type="text" cssClass="form-control"
																		path="newPassword"></form:input>
																	<form:errors cssClass="help-block" path="newPassword"></form:errors>
																	<label for="form_control_1">New Password<span
																		class="required" aria-required="true">*</span></label>
																</spring:bind>

															</div>
															<div
																class="form-group form-md-line-input form-md-floating-label">
																<spring:bind path="rNewPassword">
																	<form:input type="text" cssClass="form-control"
																		path="rNewPassword"></form:input>
																	<label for="rNewPassword">Repeat New Password<span
																		class="required" aria-required="true">*</span></label>
																	<form:errors cssClass="help-block" path="rNewPassword"></form:errors>
																</spring:bind>
															</div>

														</div>
														<div class="form-actions">
															<div class="row">
																<div class="col-md-12">
																	<button type="submit" class="btn dark">Update</button>
																	<button type="reset" class="btn default">Clear</button>
																</div>
															</div>
														</div>
													</form:form>
												</div>
												<!-- END CHANGE PASSWORD TAB -->
												<!-- PRIVACY SETTINGS TAB -->
												<div class="tab-pane" id="tab_1_5">
													<form action="#">
														<table class="table table-light table-hover">
															<tr>
																<td>Anim pariatur cliche reprehenderit, enim
																	eiusmod high life accusamus..</td>
																<td>
																	<div class="mt-radio-inline">
																		<label class="mt-radio"> <input type="radio"
																			name="optionsRadios1" value="option1" /> Yes <span></span>
																		</label> <label class="mt-radio"> <input type="radio"
																			name="optionsRadios1" value="option2" checked /> No
																			<span></span>
																		</label>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Enim eiusmod high life accusamus terry
																	richardson ad squid wolf moon</td>
																<td>
																	<div class="mt-radio-inline">
																		<label class="mt-radio"> <input type="radio"
																			name="optionsRadios11" value="option1" /> Yes <span></span>
																		</label> <label class="mt-radio"> <input type="radio"
																			name="optionsRadios11" value="option2" checked /> No
																			<span></span>
																		</label>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Enim eiusmod high life accusamus terry
																	richardson ad squid wolf moon</td>
																<td>
																	<div class="mt-radio-inline">
																		<label class="mt-radio"> <input type="radio"
																			name="optionsRadios21" value="option1" /> Yes <span></span>
																		</label> <label class="mt-radio"> <input type="radio"
																			name="optionsRadios21" value="option2" checked /> No
																			<span></span>
																		</label>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Enim eiusmod high life accusamus terry
																	richardson ad squid wolf moon</td>
																<td>
																	<div class="mt-radio-inline">
																		<label class="mt-radio"> <input type="radio"
																			name="optionsRadios31" value="option1" /> Yes <span></span>
																		</label> <label class="mt-radio"> <input type="radio"
																			name="optionsRadios31" value="option2" checked /> No
																			<span></span>
																		</label>
																	</div>
																</td>
															</tr>
														</table>
														<!--end profile-settings-->
														<div class="margin-top-10">
															<a href="javascript:;" class="btn red"> Save Changes
															</a> <a href="javascript:;" class="btn default"> Cancel </a>
														</div>
													</form>
												</div>
												<!-- END PRIVACY SETTINGS TAB -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- END PROFILE CONTENT -->
					</div>
				</div>
				<!-- END PAGE BASE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END SIDEBAR CONTENT LAYOUT -->
</div>