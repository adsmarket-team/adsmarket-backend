<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>Adsmarket | Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #5 for "
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link href="/resources/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="/resources/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link href="/resources/global/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="/resources/global/plugins/select2/css/select2.min.css" rel="stylesheet"
	type="text/css" />
<link href="/resources/global/plugins/select2/css/select2-bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="/resources/global/css/components-md.min.css" rel="stylesheet"
	id="style_components" type="text/css" />
<link href="/resources/global/css/plugins-md.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/resources/pages/css/login.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="/resources/favicon.ico" />
</head>
<!-- END HEAD -->

<body class=" login">
	<!-- BEGIN LOGO -->
	<div class="logo">
		<a href="index.html"> <img src="/resources/pages/img/logo-big.png" alt="" />
		</a>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN REGISTRATION FORM -->
		<form:form cssClass="register-form" modelAttribute="registerForm"
			action="${contextPath}/register" method="POST"
			cssStyle="display: block;">
			<h3 class="font-green">Sign Up</h3>
			<p class="hint">Enter your personal details below:</p>
			<div class="form-group">
				<spring:bind path="fullname">
					<label class="control-label visible-ie8 visible-ie9">Full
						Name</label>
					<form:input class="form-control placeholder-no-fix" type="text"
						placeholder="Full Name" path="fullname"></form:input>
					<form:errors cssClass="help-block" path="fullname"></form:errors>
				</spring:bind>
			</div>

			<div class="form-group">
				<spring:bind path="email">
					<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
					<label class="control-label visible-ie8 visible-ie9">Email</label>
					<form:input class="form-control placeholder-no-fix" type="text"
						placeholder="Email" path="email"></form:input>
					<form:errors cssClass="help-block" path="email"></form:errors>
				</spring:bind>
			</div>
			<div class="form-group">
				<spring:bind path="address">
					<label class="control-label visible-ie8 visible-ie9">Address</label>
					<form:input class="form-control placeholder-no-fix" type="text"
						placeholder="Address" path="address"></form:input>
					<form:errors cssClass="help-block" path="address"></form:errors>
				</spring:bind>
			</div>

			<p class="hint">Enter your account details below:</p>
			<div class="form-group">
				<spring:bind path="username">
					<label class="control-label visible-ie8 visible-ie9">Username</label>
					<form:input class="form-control placeholder-no-fix" type="text"
						autocomplete="off" placeholder="Username" path="username"></form:input>
					<form:errors cssClass="help-block" path="username"></form:errors>
				</spring:bind>
			</div>
			<div class="form-group">
				<spring:bind path="password">
					<label class="control-label visible-ie8 visible-ie9">Password</label>
					<form:input class="form-control placeholder-no-fix" type="password"
						autocomplete="off" id="register_password" placeholder="Password"
						path="password"></form:input>
					<form:errors cssClass="help-block" path="password"></form:errors>
				</spring:bind>
			</div>
			<div class="form-group">
				<spring:bind path="rpassword">
					<label class="control-label visible-ie8 visible-ie9">Re-type
						Your Password</label>
					<form:input class="form-control placeholder-no-fix" type="password"
						autocomplete="off" placeholder="Re-type Your Password"
						path="rpassword"></form:input>
					<form:errors cssClass="help-block" path="rpassword"></form:errors>
				</spring:bind>
			</div>
				<div class="form-group form-md-radios">
				<spring:bind path="gender">
					<label>Gender</label>
					<div class="md-radio-inline">
						<div class="md-radio">
							<form:radiobutton id="gender1" path="gender" cssClass="md-radiobtn"
								checked="checked" value="male"/> <label
								for="gender1"> <span class="inc"></span> <span
								class="check"></span> <span class="box"></span>Male
							</label>
						</div>
						<div class="md-radio">
							<form:radiobutton id="gender2" path="gender" cssClass="md-radiobtn"
								value="female"/> <label for="gender2">
								<span></span> <span class="check"></span> <span class="box"></span>
								Female
							</label>
						</div>

					</div>
				</spring:bind>
			</div>
			<div class="form-group form-md-radios">
				<spring:bind path="accountType">
					<label>Account Type</label>
					<div class="md-radio-inline">
						<div class="md-radio">
							<form:radiobutton id="accountType1" path="accountType" cssClass="md-radiobtn"
								checked="checked" value="publisher"/> <label
								for="accountType1"> <span class="inc"></span> <span
								class="check"></span> <span class="box"></span> Publisher
							</label>
						</div>
						<div class="md-radio">
							<form:radiobutton id="accountType2" path="accountType" cssClass="md-radiobtn"
								value="marketer"/> <label for="accountType2">
								<span></span> <span class="check"></span> <span class="box"></span>
								Marketer
							</label>
						</div>

					</div>
				</spring:bind>
			</div>
			<div class="form-group">
				<spring:bind path="referee">
					<label class="control-label visible-ie8 visible-ie9">Referee
						Username (Optional)))</label>
					<form:input class="form-control placeholder-no-fix" type="text"
						placeholder="Referee Username" path="referee"></form:input>
					<form:errors cssClass="help-block" path="referee"></form:errors>
				</spring:bind>
			</div>
			<div class="form-group margin-top-20 margin-bottom-20">

				<label class="mt-checkbox mt-checkbox-outline"> <input
					type="checkbox" name="tnc" /> I agree to the <a href="#">Terms
						of Service </a> & <a href="#">Privacy Policy </a> <span></span>
				</label>
				<div id="register_tnc_error"></div>
			</div>
			<div class="form-actions">
				<a href="${context}/login" type="button" id="register-back-btn"
					class="btn green btn-outline">Back</a>
				<button type="submit" id="register-submit-btn"
					class="btn btn-success uppercase pull-right">Submit</button>
			</div>
		</form:form>
		<!-- END REGISTRATION FORM -->
	</div>
	<div class="copyright">2016 � Adsmarket</div>
	<!--[if lt IE 9]>
<script src="global/plugins/respond.min.js"></script>
<script src="global/plugins/excanvas.min.js"></script> 
<script src="global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="/resources/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="/resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script src="/resources/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="/resources/global/plugins/jquery-validation/js/jquery.validate.min.js"
		type="text/javascript"></script>
	<script
		src="/resources/global/plugins/jquery-validation/js/additional-methods.min.js"
		type="text/javascript"></script>
	<script src="/resources/global/plugins/select2/js/select2.full.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="/resources/global/scripts/app.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="/resources/pages/scripts/login.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<!-- END THEME LAYOUT SCRIPTS -->
	<script type="text/javascript">
	    var countryName = '${registerForm.country}';
	</script>
</body>

</html>