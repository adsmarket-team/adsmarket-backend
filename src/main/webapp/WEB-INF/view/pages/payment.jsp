<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<div class="page-content">
	<!-- BEGIN BREADCRUMBS -->
	<div class="breadcrumbs">
		<h1>User Profile | Account</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Profile</a></li>
			<li class="active">Payment</li>
		</ol>
		<!-- Sidebar Toggle Button -->
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".page-sidebar">
			<span class="sr-only">Toggle navigation</span> <span
				class="toggle-icon"> <span class="icon-bar"></span> <span
				class="icon-bar"></span> <span class="icon-bar"></span>
			</span>
		</button>
		<!-- Sidebar Toggle Button -->
	</div>
	<!-- END BREADCRUMBS -->
	<!-- BEGIN SIDEBAR CONTENT LAYOUT -->
	<div class="page-content-container">
		<div class="page-content-row">
			<div class="page-content-col">
				<!-- BEGIN PAGE BASE CONTENT -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN PROFILE SIDEBAR -->
						<div class="profile-sidebar">
							<!-- PORTLET MAIN -->
							<div class="portlet light profile-sidebar-portlet bordered">
								<!-- SIDEBAR USERPIC -->
								<div class="profile-userpic">
									<img src="pages/media/profile/profile_user.jpg"
										class="img-responsive" alt="">
								</div>
								<!-- END SIDEBAR USERPIC -->
								<!-- SIDEBAR USER TITLE -->
								<div class="profile-usertitle">
									<div class="profile-usertitle-name">${userInfo.fullName}</div>
									<div class="profile-usertitle-job">${userInfo.accountType}</div>
								</div>
								<!-- END SIDEBAR USER TITLE -->
								<!-- SIDEBAR BUTTONS -->
								<div class="profile-userbuttons">
									<button type="button" class="btn btn-circle green btn-sm">Follow</button>
									<button type="button" class="btn btn-circle red btn-sm">Message</button>
								</div>
								<!-- END SIDEBAR BUTTONS -->
								<!-- SIDEBAR MENU -->
								<div class="profile-usermenu">
									<ul class="nav">
										<li><a href="page_user_profile_1.html"> <i
												class="icon-home"></i> Overview
										</a></li>
										<li class="active"><a
											href="page_user_profile_1_account.html"> <i
												class="icon-settings"></i> Account Settings
										</a></li>
										<li><a href="page_user_profile_1_help.html"> <i
												class="icon-info"></i> Help
										</a></li>
									</ul>
								</div>
								<!-- END MENU -->
							</div>
							<!-- END PORTLET MAIN -->
							<!-- PORTLET MAIN -->
							<div class="portlet light bordered">
								<!-- STAT -->
								<div class="row list-separated profile-stat">
									<div class="col-md-4 col-sm-4 col-xs-6">
										<div class="uppercase profile-stat-title">37</div>
										<div class="uppercase profile-stat-text">Projects</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-6">
										<div class="uppercase profile-stat-title">51</div>
										<div class="uppercase profile-stat-text">Tasks</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-6">
										<div class="uppercase profile-stat-title">61</div>
										<div class="uppercase profile-stat-text">Uploads</div>
									</div>
								</div>
								<!-- END STAT -->
								<div>
									<h4 class="profile-desc-title">About Marcus Doe</h4>
									<span class="profile-desc-text"> Lorem ipsum dolor sit
										amet diam nonummy nibh dolore. </span>
									<div class="margin-top-20 profile-desc-link">
										<i class="fa fa-globe"></i> <a
											href="http://www.keenthemes.com">www.keenthemes.com</a>
									</div>
									<div class="margin-top-20 profile-desc-link">
										<i class="fa fa-twitter"></i> <a
											href="http://www.twitter.com/keenthemes/">@keenthemes</a>
									</div>
									<div class="margin-top-20 profile-desc-link">
										<i class="fa fa-facebook"></i> <a
											href="http://www.facebook.com/keenthemes/">keenthemes</a>
									</div>
								</div>
							</div>
							<!-- END PORTLET MAIN -->
						</div>
						<!-- END BEGIN PROFILE SIDEBAR -->
						<!-- BEGIN PROFILE CONTENT -->
						<div class="profile-content">
							<div class="row">
								<div class="col-md-4">
									<div class="portlet light bordered">
										<div class="portlet-body">
											<div class="row">
												<div class="col-md-8">
													<img src="/resources/custom/img/icons/bank.svg"
														style="width: 100px; height: auto;" />
												</div>
												<div class="col-md-4 text-center">
													<input type="checkbox" class="make-switch bank" checked
														data-on-color="success" data-off-color="danger">

												</div>
											</div>
											<div class="row">

												<form:form method="POST" modelAttribute="userBankInfo">
													<div class="form-body">
														<div
															class="form-group form-md-line-input form-md-floating-label">
															<form:input type="text" cssClass="form-control"
																path="bankName"></form:input>
															<label for="bankName">Bank Name<span
																class="required" aria-required="true">*</span></label>
														</div>
														<div
															class="form-group form-md-line-input form-md-floating-label">
															<form:input type="text" cssClass="form-control"
																path="bankBranch"></form:input>
															<label for="bankBranch">Bank Branch (Optional)</label>
														</div>
														<div
															class="form-group form-md-line-input form-md-floating-label">
															<form:input type="text" cssClass="form-control"
																path="bankCountry"></form:input>
															<label for="bankCountry">Bank Country<span
																class="required" aria-required="true">*</span></label>
														</div>
														<div
															class="form-group form-md-line-input form-md-floating-label">
															<form:input type="text" cssClass="form-control"
																path="bankAccName"></form:input>
															<label for="bankAccName">Account Owner Name<span
																class="required" aria-required="true">*</span></label>
														</div>
														<div
															class="form-group form-md-line-input form-md-floating-label">
															<form:input type="text" cssClass="form-control"
																path="bankAccNo"></form:input>
															<label for="bankAccNo">Account Number<span
																class="required" aria-required="true">*</span></label>
														</div>
														<div
															class="form-group form-md-line-input form-md-floating-label">
															<form:input type="text" cssClass="form-control"
																path="swiftCode"></form:input>
															<label for="swiftCode">SWIFT Code (If bank is
																outsite of Vietnam)</label>
														</div>



													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-12">
																<button type="button" onclick="validateBankInfo()"
																	class="btn dark">Update</button>
																<button type="reset" class="btn default">Clear</button>
															</div>
														</div>
													</div>
												</form:form>
											</div>


										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="portlet light bordered">
										<div class="portlet-body">
											<div class="row">
												<div class="col-md-8">
													<img src="/resources/custom/img/icons/paypal.svg"
														style="width: 100px; height: auto;" />
												</div>
												<div class="col-md-4 text-center">
													<input type="checkbox" class="make-switch bank" checked
														data-on-color="success" data-off-color="danger">

												</div>
											</div>
											<div class="row">

												<form:form
													action="${contextPath}/profile/payment/paypalUpdate"
													method="POST" modelAttribute="paypal">
													<div class="form-body">
														<div
															class="form-group form-md-line-input form-md-floating-label">
															<form:input type="text" cssClass="form-control"
																path="paypalId"></form:input>
															<label for="paypalId">Paypal Id (Email)<span
																class="required" aria-required="true">*</span></label>
														</div>




													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-12">
																<button type="button" onclick="validatePaypal()"
																	class="btn dark">Update</button>
																<button type="reset" class="btn default">Clear</button>
															</div>
														</div>
													</div>
												</form:form>
											</div>


										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="portlet light bordered">
										<div class="portlet-body">
											<form action="#" class="form-horizontal form-bordered">
												<div class="form-body">
													<div class="form-group">
														<div class="col-md-10">
															<img src="/resources/custom/img/icons/credit-cards.svg"
																style="width: 100px; height: auto;" />
														</div>
														<div class="col-md-2">
															<input type="checkbox" class="make-switch bank"
																data-on-color="success" data-off-color="danger">

														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- END PROFILE CONTENT -->
					</div>
				</div>
				<!-- END PAGE BASE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END SIDEBAR CONTENT LAYOUT -->
</div>