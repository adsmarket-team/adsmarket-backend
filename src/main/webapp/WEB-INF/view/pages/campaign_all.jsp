<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<div class="page-content">
	<!-- BEGIN BREADCRUMBS -->
	<div class="breadcrumbs">
		<h1>Campaign</h1>
		<ol class="breadcrumb">
			<li><a href="${contextPath}/home">Home</a></li>
			<li class="active">Campaign</li>
		</ol>
	</div>
	<!-- END BREADCRUMBS -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		<form:form
			cssClass="campaign-search-form form-inline margin-bottom-10"
			modelAttribute="searchDTO"
			action="${contextPath}/campaign/multiSearch">

			<div class="form-group">

				<form:input path="name"
					cssClass="form-control input-inline input-medium"
					placeholder="App Name"></form:input>


			</div>

			<div class="form-group">
				<form:select path="type" class="form-control select2">
					<option selected="selected">Type</option>

					<option value="CPI">CPI</option>
					<option value="CPA">CPA</option>


				</form:select>

			</div>

			<div class="form-group">
				<form:select path="incentType" class="form-control select2">
					<option selected="selected">Incent Type</option>

					<option value="Incentive">Incentive</option>
					<option value="Non-Incentive">Non-Incentive</option>


				</form:select>

			</div>

			<div class="form-group">
				<form:select path="platform" class="form-control select2">
					<option selected="selected">Platform</option>

					<option value="Android">Android - Google Play</option>
					<option value="Anroid-APK">Android - APK</option>
					<option value="iOS">iOS</option>


				</form:select>

			</div>

			<div class="form-group">
				<form:select path="country" class="form-control select2">
					<option selected="selected">Country</option>

					<option value="AU">Alaska</option>
					<option value="AZ">Hawaii</option>
					<option value="CA">Canada</option>


				</form:select>

			</div>
			<div class="form-group">
				<form:select path="status" class="form-control select2">
					<option selected="selected">Status</option>

					<option value="active">Active</option>
					<option value="inactive">Inactive</option>


				</form:select>

			</div>
			<!-- 			<div class="form-group">
				<div id="campaign-price-slider" class="noUi-success"></div>
				<div id="demo8" class="noUi-danger"></div>
			</div> -->
			<div class="form-group" style="width: 300px;">
				<label class="control-label">Payout Range</label>
		
					<input id="range_3" type="text" value="" />
			
			</div>

			<button type="button" class="btn red" onclick="search()">Search</button>

		</form:form>
	</div>
	<div class="row">
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="portfolio-content portfolio-1">
			<div id="js-filters-juicy-projects" class="cbp-l-filters-button">
				<div data-filter="*"
					class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase">
					All
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".Android"
					class="cbp-filter-item btn dark btn-outline uppercase">
					Android
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".iOS"
					class="cbp-filter-item btn dark btn-outline uppercase">
					iOS
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".Incentive"
					class="cbp-filter-item btn dark btn-outline uppercase">
					Incentive
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".Non-Incentive"
					class="cbp-filter-item btn dark btn-outline uppercase">
					Non-Incentive
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".active"
					class="cbp-filter-item btn dark btn-outline uppercase">
					Active
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".inactive"
					class="cbp-filter-item btn dark btn-outline uppercase">
					Inactive
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".CPI"
					class="cbp-filter-item btn dark btn-outline uppercase">
					CPI
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".CPA"
					class="cbp-filter-item btn dark btn-outline uppercase">
					CPA
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".satisfy"
					class="cbp-filter-item btn dark btn-outline uppercase hidden searchTrigger">
					Satisfy
					<div class="cbp-filter-counter"></div>
				</div>
				<div data-filter=".none"
					class="cbp-filter-item btn dark btn-outline uppercase hidden noneTrigger">
					None
					<div class="cbp-filter-counter"></div>
				</div>

			</div>
			<div id="js-grid-juicy-projects" class="cbp">
				<c:forEach var="offerDTO" items="${listOfferDTO}">
					<div
						class="cbp-item ${offerDTO.platform} ${offerDTO.incentType} ${offerDTO.status} ${offerDTO.type} ${offerDTO.payout}">
						<div class="mt-widget-4">
							<div class="mt-img-container">
								<img src="${offerDTO.imageUrl}">
							</div>
							<div class="mt-container bg-green-opacity">
								<div class="mt-head-title item-name">${offerDTO.name}</div>
								<div class="mt-body-icons">
									<span> <c:choose>
											<c:when test="${offerDTO.platform eq 'Android' }">
												<img src="/resources/custom/img/icons/android-logo.svg"
													class="platform-icon" />
											</c:when>

											<c:otherwise>
												<img src="/resources/custom/img/icons/ios-logo.svg"
													class="platform-icon" />
											</c:otherwise>
										</c:choose>
									</span>


									<div class="campaign-content">
										<img src="/resources/custom/img/icons/global.svg" alt="region" /><span
											class="mt-head-title item-country">${offerDTO.region}</span>
									</div>
									<div class="campaign-content">
										<img src="/resources/custom/img/icons/dollar-symbol.svg" /><span
											class="mt-head-title">${offerDTO.payout}</span>
									</div>
								</div>
								<div class="mt-footer-button">
									<a type="button" class="btn btn-circle btn-danger btn-sm"
										id="${offerDTO.id}" href="#large" data-toggle="modal"
										onclick="return getOfferDetails(this)">Details</a>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>


			</div>
			<div id="js-loadMore-juicy-projects" class="cbp-l-loadMore-button">
				<a href="/resources/global/plugins/cubeportfolio/ajax/loadMore.html"
					class="cbp-l-loadMore-link btn grey-mint btn-outline"
					rel="nofollow"> <span class="cbp-l-loadMore-defaultText">LOAD
						MORE</span> <span class="cbp-l-loadMore-loadingText">LOADING...</span> <span
					class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
				</a>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
	</div>


	<!-- Section for Modal -->
	<div class="modal fade bs-modal-lg" id="large" tabindex="-1"
		role="dialog" aria-hidden="true"
		style="display: none; margin-top: 180px;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true"></button>
					<h4 class="modal-title">
						Campaign Details -<span class="offer-detail-name"></span>
					</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-3 col-xs-12 col-sm-12">

							<div class="text-center">
								<img src=""
									class="img-responsive app-detail-img-modal offer-detail-img"
									alt="">
								<div
									class="label label-danger modal-traffic-type offer-detail-incent_type"></div>
								<div class="row price-detail">
									<span class="label label-lg label-success modal-traffic-type"><img
										src="/resources/custom/img/icons/dollar-symbol.svg"
										style="width: 20px; height: auto;" /><span
										class="offer-detail-payout"></span></span>
								</div>
							</div>

						</div>
						<div class="col-lg-9 col-xs-12 col-sm-12">
							<div class="portlet light bordered">
								<div class="portlet-body">
									<div class="row">
										<div class="row text-center">
											<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
												<span class="label label-sm label-info"> Type </span>
												<h3 class="offer-detail-type"></h3>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
												<span class="label label-sm label-info"> Geo </span>
												<h3 class="offer-detail-region"></h3>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
												<span class="label label-sm label-info"> Platform </span>
												<h3 class="offer-detail-platform"></h3>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
												<span class="label label-sm label-info"> Daily Cap </span>
												<h3 class="offer-detail-cap"></h3>
											</div>
										</div>
										<div class="row requirement-container">
											<div class="mt-element-ribbon bg-grey-steel">
												<div
													class="ribbon ribbon-clip ribbon-color-warning uppercase">
													<div class="ribbon-sub ribbon-clip"></div>
													Requirement/KPI
												</div>
												<p class="ribbon-content offer-detail-requirement"></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">

						<div class="mt-clipboard-container">
							<input id="mt-target-1"
								class="form-control offer-detail-trackingLink" value=""
								type="text" readonly>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<div class="mt-body-actions">
						<div class="btn-group btn-group btn-group-justified">
							<a href="javascript:;" class="btn btn-lg mt-clipboard"
								data-clipboard-action="copy"
								data-clipboard-target="#mt-target-1"> <i class="icon-docs"></i>
								Copy Tracking Link
							</a> <a href="javascript:;" class="btn btn-lg"> <i
								class=".icon-action-redo"></i> Preview
							</a>
						</div>
					</div>

				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>



	<!-- END PAGE BASE CONTENT -->
</div>